// ====================================================================================================
//
// Cloud Code for module, write your code here to customise the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://portal.gamesparks.net/docs.htm
//
// ====================================================================================================

///PLAYER RELATED OBJECTS
// array to hold every player ID, when player ID count equals max map player count match begins
// [1, 2, ..., n]

var playerIsReady = [];
var playerIsSetup = [];
var RTPlayerArray = [];
var internalAPIKey = "9537a3f7-ed57-4c49-a453-32c7203918a1";
// Each players current draft: the draft is cleared after every spawn round.
/*
 STRUCTURE:
 
 playerDrafts: {
 1 : {
 warrior: #,
 archer : #,
 wizard : #
 },
 2 : {
 ...
 },
 ....
 n peerIDS
 }
 */
var playerDrafts = {};
// each players deck ie a summary of a units abilities / costs
/*
 STRUCTURE
 
 playerDecks : {
 0 : {
 1: {
 warrior: {
 GOLD: #,
 DAMAGE: #
 ...
 },
 archer : {
 ...
 },
 ...
 },
 2 : {
 ....
 },
 ...
 n peerIDS
 },
 ...
 n waveVersions
 }
 */
var playerDecks = {0: {}};
// each players current resource pools
/*
 playerResources : {
 1: {
 GOLD: #,
 LUMBER: #
 },
 2: {
 ...
 },
 ... n peerIDS
 }
 */
var playerResources = {};
// every players inventory: used to get the resource cost of units
/*
 playerInventory : {
 1: {
 units: {
 warrior: {
 isUpgrading: #,
 level: #
 ...
 }
 },
 weapons: {
 sword: {
 level: #
 ...
 }
 },
 armour: {
 ...
 },
 structures: {
 ...
 }
 },
 2: {
 ...
 },
 ... n peerIDS
 
 }
 */
var playerInventory = {};
/*
 
 units are anything on the board that can be owned by a player.
 structures are units that cannot move.
 npcs are units that can move.
 
 allUnits: {
 1: [
 0.peerID, -- number
 1.unitID, -- number
 2.direction, -- either a 1 or -1
 3.nodeID, -- number
 4.targetID, -- number: 0 if no target
 5.x position, -- number
 6.y position, -- number
 7.z position, -- number
 8.unitCode, -- string: corresponds to warrior, archer etc
 9.HP, -- normalised number
 10.didFire, -- number: either a 1 or 0
 11.reloadDate, -- Date.now() + unitReloadDate
 12.waveVersion, -- the waveVersion that this unit was created in
 13.buffs -- an array of currently active buffs
 14.debuffs -- an array of currently active debuffs
 15. unit size radius -- the size of that unit
 16.function ref -- the function to be used in updating this unit (used by all)
 17. targetingFunc -- function used to target other units (only used by npcs) (can also be a hard structure code. IE one that never changes, typically used to show what a construction site will turn into on completion)
 18. fireFunc -- function used when firing (only used by npcs)
 19.targetChecker -- function used to check target
 20. range -- the units range
 21. movement speed
 22. attack speed
 23. damage
 24. aoe radius
 25. hit limit
 ],
 2: [...],
 3: [...]
 ... n unitIDS
 }
 
 it is very important that peerID, unitID, targetID, x, y, z, unitCode, HP, didFire, reloadDate, waveVersion and func reference are always in the same position in the array.
 */
var allUnits = {};
// locked drafts
/*
 playerDraftsLocked: {
 1: {
 total: #, -- all the units being spawned this round
 isLocked: boolean
 },
 2: {...},
 n: {}
 }
 */
var playerDraftsLocked = {};
// dead Units
/*
 deadUnits: [ -- all the units that died in this update
 #,
 #,
 ...
 n
 ]
 */
var deadUnits = [];
// Used to assign a unique ID to each creep
var unitIDCounter = 1;
// The build sites in the map
/*
 buildSites: [
 {
 owned_by: #, -- peerID
 structure: structureCode -- corresponds to a peerID,
 completionDate,
 beginDate,
 timer,
 xQuad,
 zQuad,
 x,
 y,
 z,
 unitID,
 siteReference,
 site
 },
 {...}
 ... n]
 }
 */
var buildSites = [];
var commandCentres = [];

/*
 allStructures: [
 [   peerID,
 unitID,
 buildSite,
 completionDate,
 x,
 y,
 z,
 structureCode,
 HP,
 didFire,
 reloadDate,
 waveVersion
 ],
 [...],
 ... n
 ];
 */
var allStructures = [];
///RATES
var spawnRate = 10000;
var gameStateUpdateRate = 250;
var GoldPerUpdateCycle = 100;
var LumberPerUpdateCycle = 100;
var trapTurnsToExplosion = 3;

var spawnRateInverse;
var nextSpawnPeriod;

//GLOBAL GAME STATES
var matchIsOver;
var matchSessionBegin;
var matchIsPaused = false;
var structureCancellationPenalty = 0.5;
var lockInBonus = 0.3;
var spawnGridLength = 8;
var creepRadius = 4;
var structureRadius = 10;
var nodeRadius = 10;
var matchIsNotSetup = true;
var earnings = {
    "winner_gold": 500,
    "winner_lumber": 500,
    "loser_gold": 150,
    "loser_lumber": 150
};

//TIMER FUNCTION REFERENCES
var spawnerRef;
var unitUpdateRef;
var waveVersion = 0;

//Navigation Nodes
var allNodes = {};
//MAP ARRAY
var mapArray = {};

// ---------------------------------
//          PACKETS RECEIVED
// ---------------------------------

/*
 NOTES:
 
 playerDraftsLocked must be reset every spawn period
 playerDecks needs to be reset every spawn period
 
 */

RTSession.onPacket(100, function(packet){
    
    RTSession.newPacket()
        .setOpCode(100)
        .setReliable(false)
        .setTargetPeers(packet.getSender().getPeerId())
        .send()
    
    return false;
    
});

//player ticked ready
RTSession.onPacket(101, function(packet){
    
    RTSession.newPacket()
        .setData(RTSession.newData()
            .setNumber(1, packet.getData().getNumber(1)))
        .setOpCode(92)
        .setReliable(true)
        .setTargetPeers(packet.getSender().getPeerId())
        .send();
    
    if (playerIsReady.indexOf(packet.getSender().getPeerId()) === -1){
        playerIsReady.push(packet.getSender().getPeerId());
        
        RTSession.newRequest().createAccountDetailsRequest().setPlayerId(packet.getSender().getPlayerId()).send(function(response){
            
            RTPlayerArray.push(packet.getSender());
            
            playerDrafts[packet.getSender().getPeerId()] = {};
            playerDraftsLocked[packet.getSender().getPeerId()] = {
                "total": 0,
                "isLocked": false,
            };
            
            playerInventory[packet.getSender().getPeerId()] = {};
            
            playerDecks[waveVersion][packet.getSender().getPeerId()] = {};
            
            playerResources[packet.getSender().getPeerId()] = {
                "GOLD": response.currency1,
                "LUMBER": response.currency2
            };
            
            playerInventory[packet.getSender().getPeerId()] = response.scriptData.inventory;
            
            initialisePlayerDeck(packet.getSender().getPeerId());
            
            
            if (playerIsSetup.indexOf(packet.getSender().getPeerId()) === -1){
                playerIsSetup.push(packet.getSender().getPeerId());
            }
            
            if (playerIsSetup.length == 2){
                setupMap();
            }
            
        });
        
    }
    
    function setupMap (){
        RTSession.newPacket()
            .setOpCode(90)
            .setReliable(true)
            .send();
        
        constructMapNodes();
        constructBuildNodes();
        constructMapArray();
        setupCommandCentres();
        
        setTimeout(function(){
            // spawnerRef = RTSession.setInterval(function(){
            //     spawnUnits();
            // }, spawnRate);
            
            spawnerRef = RTSession.setInterval(spawnUnits, spawnRate);
            
            unitUpdateRef = RTSession.setInterval(updateGameState, gameStateUpdateRate);
            
            latencyUpdateRef = RTSession.setInterval(updateLatency, 1000);
            
            // unitUpdateRef = RTSession.setInterval(function(){
            //     updateGameState();
            // }, gameStateUpdateRate);
            
            respondToClient.sendSpawnTimer(spawnRate);
            
            // RTSession.newPacket()
            //     .setOpCode(91)
            //     .setData(RTSession.newData()
            //                 .setNumber(1, spawnRate))
            //     .setReliable(true)
            //     .send();
            
        }, 5000);
        
    }
    
    return packet;
    
});

//player is drafting a new unit
RTSession.onPacket(102, function(packet){
    
    var peerID = packet.getSender().getPeerId();
    var unitName = packet.getData().getString(1);
    
    if (!playerDraftsLocked[peerID].isLocked){ // check for unit being locked
        
        if (Object.keys(playerDecks[waveVersion][peerID]).indexOf(unitName) !== -1){ // check player is allowed to draft unit
            var currentGold = playerResources[peerID].GOLD;
            var currentLumber = playerResources[peerID].LUMBER;
            
            var unitGoldCost = playerDecks[waveVersion][peerID][unitName].GOLD;
            var unitLumberCost = playerDecks[waveVersion][peerID][unitName].LUMBER;
            
            if (currentGold >= unitGoldCost && currentLumber >= unitLumberCost){
                
                playerResources[peerID].GOLD -= unitGoldCost;
                playerResources[peerID].LUMBER -= unitLumberCost;
                
                if (!playerDrafts[peerID][unitName]){
                    playerDrafts[peerID][unitName] = 0;
                }
                
                playerDrafts[peerID][unitName]++;
                
                RTSession.newPacket()
                    .setOpCode(202)
                    .setData(RTSession.newData()
                        .setString(1, unitName))
                    .setReliable(true)
                    .setTargetPeers(packet.getSender().getPeerId())
                    .send(); // send a packet to sender confirming unit addition and new resource count
                
                respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
                
                playerDraftsLocked[peerID].total++;
                var sendTo = playerIsReady.slice(0);
                sendTo.splice(sendTo.indexOf(peerID), 1);
                
                RTSession.newPacket()
                    .setOpCode(203)
                    .setData(RTSession.newData()
                        .setNumber(1, peerID)
                        .setNumber(2, playerDraftsLocked[peerID].total))
                    .setTargetPeers(sendTo)
                    .setReliable(true)
                    .send(); // send a packet to peers indicating new unlocked unit count
                
            } else {
                respondToClient.sendInsufficientResourcesToPeer(peerID);
            }
            
        }  else {
            respondToClient.sendInsufficientResourcesToPeer(peerID);
        }
    }  else {
        respondToClient.sendInsufficientResourcesToPeer(peerID);
    }
    
    return false;
    
});

//remove a drafted unit from a player
RTSession.onPacket(103, function(packet){
    
    var peerID = packet.getSender().getPeerId();
    var unitCode = packet.getData().getString(1);
    
    if (!playerDraftsLocked[peerID].isLocked &&
        Object.keys(playerDecks[waveVersion][peerID]).indexOf(unitCode) !== -1 ){ // check for unit being locked and existing
        
        if (playerDrafts[peerID][unitCode] > 0){
            playerDrafts[peerID][unitCode] -= 1;
            
            var unitGoldCost = playerDecks[waveVersion][peerID][unitCode].GOLD;
            var unitLumberCost = playerDecks[waveVersion][peerID][unitCode].LUMBER;
            
            playerResources[peerID].GOLD += unitGoldCost;
            playerResources[peerID].LUMBER += unitLumberCost;
            
            RTSession.newPacket()
                .setOpCode(303)
                .setData(RTSession.newData()
                    .setString(1, unitCode))
                .setReliable(true)
                .setTargetPeers(packet.getSender().getPeerId())
                .send(); // send a packet to sender confirming unit addition and new resource count
            
            respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
            
            playerDraftsLocked[peerID].total--;
            var sendTo = playerIsReady.slice(0);
            sendTo.splice(sendTo.indexOf(peerID), 1);
            
            RTSession.newPacket()
                .setOpCode(203)
                .setData(RTSession.newData()
                    .setNumber(1, peerID)
                    .setNumber(2, playerDraftsLocked[peerID].total))
                .setTargetPeers(sendTo)
                .setReliable(true)
                .send(); // send a packet to peers indicating new unlocked unit count
            
        }  else {
            respondToClient.sendNoUnitsToRemoveToPeer(peerID);
        }
        
    }  else {
        respondToClient.sendNoUnitsToRemoveToPeer(peerID);
    }
    
    return false;
    
});

//player is locking in units
RTSession.onPacket(104, function(packet){
    
    var peerID = packet.getSender().getPeerId();
    
    // RTSession.getLogger().debug(peerID);
    // RTSession.getLogger().debug(unitCode);
    // RTSession.getLogger().debug(playerDrafts);
    // RTSession.getLogger().debug(playerDrafts[peerID]);
    // RTSession.getLogger().debug(playerDrafts[peerID][unitCode]);
    // RTSession.getLogger().debug(playerDraftsLocked[peerID]);
    
    if (Object.keys(playerDrafts[peerID]).length > 0 &&
        !playerDraftsLocked[peerID].isLocked){
        
        playerDraftsLocked[peerID].isLocked = true;
        
        var goldDiscount = 0;
        var lumberDiscount = 0;
        
        Object.keys(playerDrafts[peerID]).forEach(function(unitCode){
            
            if (playerDrafts[peerID][unitCode] <= 0){
                return;
            }
            
            var goldCost = playerDecks[waveVersion][peerID][unitCode].GOLD;
            var lumberCost = playerDecks[waveVersion][peerID][unitCode].LUMBER;
            var unitCount = playerDrafts[peerID][unitCode];
            
            goldDiscount += ~~(goldCost * unitCount * lockInBonus * spawnRateInverse * (nextSpawnPeriod - Date.now()));
            lumberDiscount += ~~(lumberCost * unitCount * lockInBonus * spawnRateInverse * (nextSpawnPeriod - Date.now()));
            
        });
        
        playerResources[peerID].GOLD += goldDiscount;
        playerResources[peerID].LUMBER += lumberDiscount;
        
        RTSession.newPacket()
            .setOpCode(204)
            .setData(RTSession.newData()
                .setNumber(1, goldDiscount)
                .setNumber(2, lumberDiscount))
            .setReliable(true)
            .setTargetPeers(peerID)
            .send();
        
        respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
        
        var sendTo = playerIsReady.slice(0);
        sendTo.splice(sendTo.indexOf(peerID), 1);
        
        var packetData = RTSession.newData();
        var packetCode = 1;
        
        packetData.setNumber(packetCode++, peerID);
        
        Object.keys(playerDrafts[peerID]).forEach(function (unitCode) {
            
            packetData.setString(packetCode++, unitCode);
            packetData.setNumber(packetCode++, playerDrafts[peerID][unitCode]);
            
        });
        
        RTSession.newPacket()
            .setOpCode(211)
            .setData(packetData)
            .setReliable(true)
            .setTargetPeers(sendTo)
            .send()
        
    } else {
        respondToClient.sendNoUnitsToRemoveToPeer(peerID);
    }
    
    return false;
    
});

// player is requesting a new structure be built.
RTSession.onPacket(106, function(packet){
    
    var peerID = packet.getSender().getPeerId();
    var structureCode = packet.getData().getString(1);
    var siteNumber = packet.getData().getNumber(2);
    var buildSite = buildSites[siteNumber];
    
    // RTSession.getLogger().debug("peerID " + peerID + "\nstructureCode " + structureCode + "\nsitenumber " + siteNumber + "\nbuildSite " + buildSite);
    
    // RTSession.getLogger().debug(playerDecks[waveVersion][peerID][structureCode]);
    // RTSession.getLogger().debug(buildSite);
    // RTSession.getLogger().debug(buildSite.owned_by);
    // RTSession.getLogger().debug(buildSite.structure);
    
    if (playerDecks[waveVersion][peerID][structureCode] &&
        buildSite &&
        buildSite.owned_by === peerID &&
        buildSite.structure === "none"){
        
        RTSession.getLogger().debug("packet validated");
        
        if (playerResources[peerID].GOLD >= playerDecks[waveVersion][peerID][structureCode].GOLD &&
            playerResources[peerID].LUMBER >= playerDecks[waveVersion][peerID][structureCode].LUMBER){
            
            RTSession.getLogger().debug("resources validated");
            
            buildSite.structure = structureCode;
            buildSite.completionDate = (playerDecks[waveVersion][peerID][structureCode].CONSTRUCTION_TIME * gameStateUpdateRate) + Date.now();
            buildSite.beginDate = Date.now();
            
            RTSession.getLogger().debug(playerDecks[waveVersion][peerID][structureCode].CONSTRUCTION_TIME);
            RTSession.getLogger().debug(playerDecks[waveVersion][peerID][structureCode].HP);
            RTSession.getLogger().debug(playerDecks[waveVersion][peerID][structureCode]);
            
            allUnits[unitIDCounter] = [peerID,
                unitIDCounter,
                0,
                buildSite.siteReference,
                0,
                buildSite.x,
                buildSite.y,
                buildSite.z,
                "construction_site",
                playerDecks[waveVersion][peerID][structureCode].HP * (1 / playerDecks[waveVersion][peerID][structureCode].CONSTRUCTION_TIME),
                0,
                0,
                waveVersion,
                [],
                [],
                structureRadius,
                buildStructure,
                structureCode];
            
            RTSession.getLogger().debug("adding structure to allUnits");
            
            playerResources[peerID].GOLD -= playerDecks[waveVersion][peerID][structureCode].GOLD;
            playerResources[peerID].LUMBER -= playerDecks[waveVersion][peerID][structureCode].LUMBER;
            
            RTSession.getLogger().debug("updating player resources");
            
            buildSite.unitID = unitIDCounter++;
            
            mapArray[buildSite.xQuad][buildSite.zQuad][peerID].push(buildSite.unitID);
            
            respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
            
            RTSession.getLogger().debug("building structure");
            
            // RTSession.newPacket()
            //     .setOpCode(opCode)
            
            // RTSession.newPacket()
            //     .setOpCode(205)
            //     .setData(RTSession.newData()
            //         .setNumber(1, peerID)
            //         .setNumber(2, buildSite.unitID)
            //         .setNumber(3, siteNumber)
            //         .setNumber(4, buildSite.beginDate)
            //         .setNumber(5, buildSite.x)
            //         .setNumber(6, buildSite.y)
            //         .setNumber(7, buildSite.z)
            //         .setString(8, "construction_site")
            //         .setNumber(9, playerDecks[waveVersion][peerID][structureCode].HP)
            //         .setNumber(10, 0)
            //         .setNumber(11, 0)
            //         .setNumber(12, waveVersion))
            //     .setReliable(true)
            //     .send();
            
        } else {
            respondToClient.sendInsufficientResourcesToPeer(peerID);
        }
        
    } else {
        respondToClient.sendInsufficientResourcesToPeer(peerID);
    }
    
    return false;
    
});

// player is cancelling a structure
RTSession.onPacket(107, function(packet){
    
    var peerID = packet.getSender().getPeerId();
    var siteNumber = packet.getData().getNumber(1);
    var buildSite = buildSites[siteNumber];
    
    if (buildSite &&
        buildSite.owned_by == peerID &&
        buildSite.completionDate >= Date.now()){
        
        // console.log("BGOLD: " + playerResources[peerID].GOLD);
        // console.log("BLUMBER: " + playerResources[peerID].LUMBER);
        
        playerResources[peerID].GOLD += ~~(playerDecks[waveVersion][peerID][buildSites[siteNumber].structure].GOLD * structureCancellationPenalty);
        playerResources[peerID].LUMBER += ~~(playerDecks[waveVersion][peerID][buildSites[siteNumber].structure].LUMBER * structureCancellationPenalty);
        
        // console.log("AGOLD: " + playerResources[peerID].GOLD);
        // console.log("ALUMBER: " + playerResources[peerID].LUMBER);
        
        respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
        
        buildSite.structure = "none";
        
        mapArray[buildSite.xQuad][buildSite.zQuad][peerID].splice(mapArray[buildSite.xQuad][buildSite.zQuad][peerID].indexOf(buildSite.unitID), 1);
        
        clearTimeout(buildSite.timer);
        
        delete allUnits[buildSite.unitID];
        delete buildSite.completionDate;
        delete buildSite.beginDate;
        
        RTSession.newPacket()
            .setOpCode(206)
            .setData(RTSession.newData()
                .setNumber(1, peerID)
                .setNumber(2, buildSite.unitID)
                .setNumber(3, buildSite.siteReference))
            .setReliable(true)
            .send();
        
    } else {
        respondToClient.sendNoUnitsToRemoveToPeer(peerID);
    }
    
    return false;
    
});

//player is changing a units equipment
RTSession.onPacket(108, function(packet){
    
    RTSession.getLogger().debug("packet 108 received");
    
    var peerID = packet.getSender().getPeerId();
    var unitCode = packet.getData().getString(1);
    var weaponCode = packet.getData().getString(2);
    var armourCode = packet.getData().getString(3);
    
    if (!playerDraftsLocked[peerID].isLocked){
        
        if (playerInventory[peerID].units[unitCode] !== undefined &&
            playerInventory[peerID].weapons[weaponCode] !== undefined &&
            playerInventory[peerID].armour[armourCode] !== undefined &&
            playerInventory[peerID].units[unitCode].level !== 0 &&
            playerInventory[peerID].armour[armourCode].level !== 0 &&
            playerInventory[peerID].weapons[weaponCode].level !== 0 &&
            (playerInventory[peerID].units[unitCode].weapon !== weaponCode ||
            playerInventory[peerID].units[unitCode].armour !== armourCode)){
            
            var weaponBaseStats = getItemStats(weaponCode);
            var armourBaseStats = getItemStats(armourCode);
            var unitBaseStats = getUnitStats(unitCode, playerInventory[peerID].units[unitCode]);
            
            var newUnit = {
                "GOLD": weaponBaseStats.GOLD + armourBaseStats.GOLD + unitBaseStats.GOLD,
                "LUMBER": weaponBaseStats.LUMBER + armourBaseStats.LUMBER + unitBaseStats.LUMBER,
                "HP": weaponBaseStats.HP + armourBaseStats.HP + unitBaseStats.HP,
                "RANGE": weaponBaseStats.RANGE + armourBaseStats.RANGE + unitBaseStats.RANGE,
                "MOVEMENT_SPEED": weaponBaseStats.MOVEMENT_SPEED + armourBaseStats.MOVEMENT_SPEED + unitBaseStats.MOVEMENT_SPEED,
                "ATTACK_SPEED": weaponBaseStats.ATTACK_SPEED + armourBaseStats.ATTACK_SPEED + unitBaseStats.ATTACK_SPEED,
                "HIT_LIMIT": weaponBaseStats.HIT_LIMIT + armourBaseStats.HIT_LIMIT + unitBaseStats.HIT_LIMIT,
                "AOE_RADIUS": weaponBaseStats.AOE_RADIUS + armourBaseStats.AOE_RADIUS + unitBaseStats.AOE_RADIUS,
                "DAMAGE": weaponBaseStats.DAMAGE + armourBaseStats.DAMAGE + unitBaseStats.DAMAGE
            };
            
            var unitCount = playerDrafts[peerID][unitCode];
            
            if (!unitCount){
                unitCount = 0;
            }
            
            var goldCost = newUnit.GOLD * unitCount;
            var lumberCost = newUnit.LUMBER * unitCount;
            
            var oldGoldCost = playerDecks[waveVersion][peerID][unitCode].GOLD * unitCount;
            var oldLumberCost = playerDecks[waveVersion][peerID][unitCode].LUMBER * unitCount;
            
            var dGold = goldCost - oldGoldCost;
            var dLumber = lumberCost - oldLumberCost;
            
            if (dGold <= playerResources[peerID].GOLD &&
                dLumber <= playerResources[peerID].LUMBER){
                
                playerDecks[waveVersion + 1] = JSON.parse(JSON.stringify(playerDecks[waveVersion]));
                playerDecks[++waveVersion][peerID][unitCode] = newUnit;
                
                playerResources[peerID].GOLD -= dGold;
                playerResources[peerID].LUMBER -= dLumber;
                
                var packetData = RTSession.newData();
                
                packetData.setString(1, unitCode);
                packetData.setString(2, weaponCode);
                packetData.setString(3, armourCode);
                packetData.setNumber(4, dGold);
                packetData.setNumber(5, dLumber);
                
                
                playerInventory[peerID].units[unitCode].weapon = weaponCode;
                playerInventory[peerID].units[unitCode].armour = armourCode;
                
                RTSession.newPacket()
                    .setOpCode(308)
                    .setData(packetData)
                    .setTargetPeers(peerID)
                    .setReliable(true)
                    .send();
                
            }
        }
    }
    
    return false;
    
});

RTSession.onPlayerDisconnect(function(player){
    
    if (matchIsOver){
        return;
    }
    
    var winner = player.getPeerId() === 1 ? 2 : 1;
    
    RTSession.clearInterval(spawnerRef);
    RTSession.clearInterval(unitUpdateRef);
    
    RTSession.newPacket()
        .setOpCode(10)
        .setData(RTSession.newData()
            .setNumber(1, winner)
            .setNumber(2, 500)
            .setNumber(3, 500))
        .setTargetPeers(winner)
        .setReliable(true)
        .send();
    
    matchIsOver = true;
    
});

// ---------------------------------
//          GAME STATE CENTRAL
// ---------------------------------

function updateLatency(){
    
    var timeStamp = Date.now();
    
    RTSession.newPacket()
        .setData(RTSession.newData()
            .setNumber(1, timeStamp))
        .setOpCode(99)
        .setReliable(false)
        .send();
    
}

function updateGameState(){
    
    var unitData = RTSession.newData();
    deadUnits = [];
    var buffer = 0;
    
    var unitIDS = Object.keys(allUnits);
    
    for(var i = 0; i < unitIDS.length; i++){ //update all units & structures
        
        var unit = allUnits[unitIDS[i]]; // get the data array for that unit
        
        allUnits[unitIDS[i]] = unit[16](unit); // run the referenced function to update the unit and store the update
        
        //  allUnits[unitIDS[i]][16](allUnits[unitIDS[i]]);
        
        unitData.setString(++buffer,  allUnits[unitIDS[i]].slice(0, 14).join(":")); // add the updated unit data into the packet buffer
        
        if (buffer == 10 || i == (unitIDS.length - 1)){
            RTSession.newPacket()
                .setOpCode(201)
                .setData(unitData)
                .send();
            buffer = 0;
            unitData = RTSession.newData();
        }
        
    }
    
    RTSession.getLogger().debug("no fatal error after unit update");
    
    // update build sites
    buildSites.forEach(function(site){
        
        if (site.structure === "none"){
            
            var peer1IsNearby = scanForTarget(1, site.xQuad, site.zQuad);
            var peer2IsNearby = scanForTarget(2, site.xQuad, site.zQuad);
            
            if (peer1IsNearby !== peer2IsNearby){
                
                var newOwner = peer1IsNearby ? 1 : 2;
                
                if (newOwner !== site.owned_by){
                    
                    site.owned_by = newOwner;
                    
                    RTSession.newPacket()
                        .setOpCode(209)
                        .setData(RTSession.newData()
                            .setNumber(1, site.owned_by)
                            .setNumber(2, site.siteReference))
                        .setReliable(true)
                        .send();
                    
                }
                
            }
            
        }
        
    });
    
    RTSession.getLogger().debug("no fatal error after buildiste update");
    
    updateUnitManifest();
    
    if (deadUnits.length != 0){
        
        var packetData = RTSession.newData().setNumber(1, deadUnits.length);
        
        for(var i = 0; i < deadUnits.length; i++){
            packetData.setNumber(i + 2, deadUnits[i]);
        }
        
        RTSession.newPacket()
            .setOpCode(207)
            .setData(packetData)
            .setReliable(true)
            .send();
    }
    
    RTSession.getLogger().debug("no fatal error after dead unit update");
    
    playerIsReady.forEach(function (peerID) {
        respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
    });
    
    RTSession.getLogger().debug("no fatal error after resource update");
    
    var peer1 = allUnits[commandCentres[0].unitID];
    var peer2 = allUnits[commandCentres[1].unitID];
    
    
    if (peer1 === undefined && peer2 === undefined){
        
        var drawGold = 250;
        var drawLumber = 250;
        
        RTSession.newPacket()
            .setOpCode(11)
            .setData(RTSession.newData()
                .setNumber(1, drawGold)
                .setNumber(2, drawLumber))
            .setReliable(true)
            .send();
        
    } else if (peer1 === undefined || peer2 === undefined){
        
        var winner = peer1 === undefined ? 2 : 1;
        var loser = peer1 === undefined ? 1 : 2;
        
        var winnerGold = earnings.winner_gold;
        var winnerLumber = earnings.winner_lumber;
        
        var loserGold = earnings.loser_gold;
        var loserLumber = earnings.loser_lumber;
        
        RTSession.clearInterval(spawnerRef);
        RTSession.clearInterval(unitUpdateRef);
        
        RTSession.newPacket()
            .setOpCode(10)
            .setData(RTSession.newData()
                .setNumber(1, winner)
                .setNumber(2, winnerGold)
                .setNumber(3, winnerLumber))
            .setTargetPeers(winner)
            .setReliable(true)
            .send();
        
        RTSession.newPacket()
            .setOpCode(10)
            .setData(RTSession.newData()
                .setNumber(1, winner)
                .setNumber(2, loserGold)
                .setNumber(3, loserLumber))
            .setTargetPeers(loser)
            .setReliable(true)
            .send();
        
        matchIsOver = true;
        
        gameover.calculateEarnings(RTPlayerArray[0].getPlayerId(), RTPlayerArray[1].getPlayerId(), RTPlayerArray[0].getPeerId() === winner);
        
    }
    
    RTSession.getLogger().debug("no fatal error at end");
    
}

/*
 ---------------------------------
 CORE
 ---------------------------------
 */

function spawnUnits(){
    
    nextSpawnPeriod += spawnRate;
    
    playerIsReady.forEach(function(peerID){
        
        var x = peerID == 1 ? 405.6 : 100.8;
        var y = 25;
        var z = peerID == 1 ? 39.8 : 416.3;
        var direction = peerID == 1 ? -1 : 1;
        var firstNode = peerID == 1 ? 5 : 1;
        var opposingPlayerID = peerID == 1 ? 2 : 1;
        var unitData = RTSession.newData();
        var countRef = 1;
        playerDraftsLocked[peerID].isLocked = false;
        
        Object.keys(playerInventory[peerID].units).forEach(function(unitCode){
            
            unitData.setString(countRef++, unitCode);
            unitData.setString(countRef++, playerInventory[opposingPlayerID].units[unitCode].weapon);
            unitData.setString(countRef++, playerInventory[opposingPlayerID].units[unitCode].armour);
            unitData.setNumber(countRef++, peerID);
            
        }); // tells the client what the unit is like for each peer.
        
        RTSession.newPacket()
            .setOpCode(208)
            .setData(unitData)
            .setReliable(true)
            .send();
        
        var zRow = 0;
        
        var units = Object.keys(playerDrafts[peerID]);
        
        if (units.length > 0){
            
            units.forEach(function(unitCode){
                
                RTSession.getLogger().debug(unitIDCounter);
                
                var attackFunction = getAttackFunction(peerID, unitCode);
                var targetFunction = getTargetFunction(unitCode);
                var updateFunction = getUpdateFunction(unitCode);
                var targetCheckerFunction = getTargetCheckerFunction(unitCode);
                
                for (var i = 0; i < playerDrafts[peerID][unitCode]; i++){
                    
                    allUnits[unitIDCounter] = [peerID,                              //0
                        unitIDCounter,                                              //1
                        direction,                                                  //2
                        firstNode,                                                  //3
                        0,                                                          //4
                        x,                                                          //5
                        y,                                                          //6
                        z,                                                          //7
                        unitCode,                                                   //8
                        playerDecks[waveVersion][peerID][unitCode].HP,              //9
                        0,                                                          //10
                        0,                                                          //11
                        waveVersion,                                                //12
                        [],                                                         //13
                        [],                                                         //14
                        creepRadius,                                                //15
                        updateFunction,                                             //16
                        targetFunction,                                             //17
                        attackFunction,                                             //18
                        targetCheckerFunction,                                      //19
                        playerDecks[waveVersion][peerID][unitCode].RANGE,           //20
                        playerDecks[waveVersion][peerID][unitCode].MOVEMENT_SPEED,  //21
                        playerDecks[waveVersion][peerID][unitCode].ATTACK_SPEED,    //22
                        playerDecks[waveVersion][peerID][unitCode].DAMAGE,          //23
                        playerDecks[waveVersion][peerID][unitCode].AOE_RADIUS,      //24
                        playerDecks[waveVersion][peerID][unitCode].HIT_LIMIT];      //25
                    
                    mapArray[~~(x * 0.0625)][~~(z * 0.0625)][peerID].push(unitIDCounter++);
                    
                    
                    
                    // spawns units in a 8 x n grid
                    if (zRow == spawnGridLength){
                        zRow = 0;
                        z += 6;
                        x -= (spawnGridLength * 2);
                    } else {
                        x += 6;
                        zRow++;
                    }
                    
                }
                
            });
        }
        
        // reset
        playerDrafts[peerID] = {};
        playerDraftsLocked[peerID] = {
            "total": 0,
            "isLocked": false
        };
        
    });
    
    respondToClient.sendSpawnTimer(spawnRate);
    
}

function updateUnitManifest(){
    
    var unitIDS = Object.keys(allUnits);
    deadUnits = [];
    
    for (var i = 0; i < unitIDS.length; i++){
        if (allUnits[unitIDS[i]][9] <= 0){
            deadUnits.push(unitIDS[i]);
            
            var xQuad = ~~(allUnits[unitIDS[i]][5] * 0.0625);
            var zQuad = ~~(allUnits[unitIDS[i]][7] * 0.0625);
            
            var peerID = allUnits[unitIDS[i]][0];
            
            // console.log("xQuad: " + xQuad);
            // console.log("zQuad: " + zQuad);
            // console.log("peerID: " + peerID);
            // console.log("unitID: " + unitIDS[i]);
            
            // console.log("array of units in QUAD: " + mapArray[xQuad][zQuad][peerID]);
            // console.log("index at: " + mapArray[xQuad][zQuad][peerID].indexOf(allUnits[unitIDS[i]][1]));
            
            mapArray[xQuad][zQuad][peerID].splice(mapArray[xQuad][zQuad][peerID].indexOf(allUnits[unitIDS[i]][1]), 1);
            
            delete allUnits[unitIDS[i]];
        }
    }
    
}

function updateUnit(unitReference){
    
    var dataArray = unitReference.slice(0);
    dataArray[10] = 0;
    
    // if 1 going forwards through map
    // if 0 we are targeting a creep already.
    // if -1 going backwards through map
    if (isTargetingWaypoint(dataArray)){ // targeting a waypoint
        
        var target = executeTargetSearcher(dataArray); // invoke custom unit target finder function
        
        console.log("target found!: " + target);
        
        if (target){ // found a target
            
            setUnitTarget(target, dataArray);
            setTargetTypeToCreep(dataArray);
            
            if (isInRange(dataArray)){
                
                if (notReloading(dataArray)){
                    
                    executeAttack(dataArray);
                    
                }
                
            } else {
                
                dataArray = moveToTarget(dataArray);
                
            }
            
        } else {
            
            dataArray = moveToNode(dataArray);
            
        }
        
    } else {
        
        if (executeTargetChecker(dataArray)){ // custom unit target checker function
            
            if (isInRange(dataArray)){
                
                if (notReloading(dataArray)){
                    
                    executeAttack(dataArray);
                    
                }
                
            } else {
                
                dataArray = moveToTarget(dataArray);
                
            }
            
        } else {
            
            var target = executeTargetSearcher(dataArray); // custom unit targeting function
            
            if (target){ // found a target
                
                setUnitTarget(target, dataArray);
                
                if (isInRange(dataArray)){
                    
                    if (notReloading(dataArray)){
                        
                        executeAttack(dataArray);
                        
                    }
                    
                } else {
                    
                    dataArray = moveToTarget(dataArray);
                    
                }
                
            } else {
                
                setTargetTypeToWaypoint(dataArray);
                dataArray = moveToNode(dataArray);
                
            }
        }
    }
    
    return dataArray;
    
}

function updateMonk(unitArray) {
    
    console.log("updating monk...");
    
    executeAttack(unitArray);
    
    if (isTargetingWaypoint(unitArray)){
        
        var target = executeTargetSearcher(unitArray);
        
        if (target){
            
            setUnitTarget(target, unitArray);
            setTargetTypeToCreep(unitArray);
            
            if (isInRange(unitArray)){
                
                if (notReloading(unitArray)){
                    basicAttack(unitArray);
                }
                
            } else {
                
                moveToTarget(unitArray);
                
            }
            
        } else {
            
            moveToNode(unitArray);
            
        }
        
    } else {
        
        if (executeTargetChecker(unitArray)){
            
            if (isInRange(unitArray)){
                
                if (notReloading(unitArray)){
                    basicAttack(unitArray);
                }
                
            } else {
                
                moveToTarget(unitArray);
                
            }
            
        } else {
            
            var target = executeTargetSearcher(unitArray);
            
            if (target){
                
                setUnitTarget(target, unitArray);
                setTargetTypeToCreep(unitArray);
                
                if (isInRange(unitArray)){
                    
                    basicAttack(unitArray);
                    
                } else {
                    
                    moveToTarget(unitArray);
                    
                }
                
                
            } else {
                
                setTargetTypeToWaypoint(unitArray);
                moveToNode(unitArray);
                
            }
            
        }
        
    }
    
    return unitArray;
    
}

function updateStructure(structureArray){
    
    // console.log("updating structure...");
    
    var peerID = getUnitPeerID(structureArray);
    var structureCode = getUnitCode(structureArray);
    
    structureArray[10] = 0;
    
    playerDecks[getUnitWaveVersion(structureArray)][peerID][structureCode].ABILITY(structureArray);
    
    return structureArray;
    
}

function buildStructure (structureArray) {
    
    // console.log("building structure...");
    
    // RTSession.getLogger().debug(structureArray);
    // RTSession.getLogger().debug(getUnitWaveVersion(structureArray));
    // RTSession.getLogger().debug(getUnitPeerID(structureArray));
    // RTSession.getLogger().debug(getUnitCode(structureArray));
    // RTSession.getLogger().debug(playerDecks[getUnitWaveVersion(structureArray)][getUnitPeerID(structureArray)][getUnitCode(structureArray)].CONSTRUCTION_TIME);
    // RTSession.getLogger().debug(getUnitHP(structureArray));
    
    var totalHP = playerDecks[getUnitWaveVersion(structureArray)][getUnitPeerID(structureArray)][getHardStructureCode(structureArray)].HP;
    var constructionTime = playerDecks[getUnitWaveVersion(structureArray)][getUnitPeerID(structureArray)][getHardStructureCode(structureArray)].CONSTRUCTION_TIME;
    var hpDifference = totalHP - getUnitHP(structureArray);
    var hpBuild = totalHP * (1 / constructionTime);
    //
    // console.log("currentHP: " + structureArray[9]);
    // console.log("totalHP: " + totalHP);
    RTSession.getLogger().debug("constructionTime: " + constructionTime);
    // console.log("hpDifference: " + hpDifference);
    // console.log("hpBuild: " + hpBuild);
    
    structureArray[9] += hpDifference < hpBuild ? hpDifference : hpBuild;
    
    RTSession.getLogger().debug("newHP: " + structureArray[9]);
    
    
    if (getUnitHP(structureArray) >= totalHP){
        structureArray[16] = updateStructure;
        structureArray[8] = getHardStructureCode(structureArray);
        RTSession.getLogger().debug("updating update func");
        
        RTSession.newPacket()
            .setOpCode(212)
            .setData(RTSession.newData()
                .setNumber(1, getUnitPeerID(structureArray))
                .setNumber(2, getUnitID(structureArray))
                .setString(3, structureArray[17]))
            .setReliable(true)
            .send();
    }
    
    RTSession.getLogger().debug(structureArray);
    
    return structureArray;
}

function constructBuildNodes(){
    
    buildSites = [
        {
            "x": 302,
            "y": 25,
            "z": 446.2,
            "site": "buildable",
            "owned_by": 0,
            "structure": "none",
            "xQuad": ~~(302 * 0.0625),
            "zQuad": ~~(446.2 * 0.0625),
            "siteReference": 0
        },
        {
            "x": 427.4,
            "y": 25,
            "z": 358.1,
            "site": "buildable",
            "owned_by": 0,
            "structure": "none",
            "xQuad": ~~(427.4 * 0.0625),
            "zQuad": ~~(358.1 * 0.0625),
            "siteReference": 1
        },
        {
            "x": 256.6,
            "y": 25,
            "z": 236.1,
            "site": "buildable",
            "owned_by": 0,
            "structure": "none",
            "xQuad": ~~(256.6 * 0.0625),
            "zQuad": ~~(236.1 * 0.0625),
            "siteReference": 2
        },
        {
            "x": 144.6,
            "y": 25,
            "z": 59.4,
            "site": "buildable",
            "owned_by": 0,
            "structure": "none",
            "xQuad": ~~(144.6 * 0.0625),
            "zQuad": ~~(59.4 * 0.0625),
            "siteReference": 3
        },
        {
            "x": 256.7,
            "y": 25,
            "z": 87.3,
            "site": "buildable",
            "owned_by": 0,
            "structure": "none",
            "xQuad": ~~(256.7 * 0.0625),
            "zQuad": ~~(87.3 * 0.0625),
            "siteReference": 4
        }];
}

function constructMapNodes(){
    allNodes = {
        "0": {
            "x": 100.8,
            "y": 25,
            "z": 416.3,
            "site": "capital"
        },
        "1": {
            "x": 302,
            "y": 25,
            "z": 446.2,
            "site": "buildable",
            "build_site": 0
        },
        "2": {
            "x": 427.4,
            "y": 25,
            "z": 358.1,
            "site": "buildable",
            "build_site": 1
        },
        "3": {
            "x": 256.6,
            "y": 25,
            "z": 236.1,
            "site": "buildable",
            "build_site": 2
        },
        "4": {
            "x": 144.6,
            "y": 25,
            "z": 59.4,
            "site": "buildable",
            "build_site": 3
        },
        "5": {
            "x": 256.7,
            "y": 25,
            "z": 87.3,
            "site": "buildable",
            "build_site": 4
        },
        "6": {
            "x": 405.6,
            "y": 25,
            "z": 39.8,
            "site": "capital"
        }
    };
}

function constructMapArray(){
    for(var i = 0; i < 32; i++){
        mapArray[i] = {};
        for(var j = 0; j < 32; j++){
            
            mapArray[i][j] = {};
            
            playerIsReady.forEach(function(playerID){
                
                mapArray[i][j][playerID] = [];
                
            });
            
        }
    }
}

function setupCommandCentres() {
    
    commandCentres[0] = {
        "x": 405.6,
        "y": 25,
        "z": 39.8,
        "unitID": unitIDCounter++,
        "xQuad": ~~(405.6 * 0.0625),
        "zQuad": ~~(39.8 * 0.0625),
        "peerID": 1
    };
    
    commandCentres[1] = {
        "x": 100.8,
        "y": 25,
        "z": 416.3,
        "unitID": unitIDCounter++,
        "xQuad": ~~(100.8 * 0.0625),
        "zQuad": ~~(416.3 * 0.0625),
        "peerID": 2
    };
    
    mapArray[commandCentres[0].xQuad][commandCentres[0].zQuad][commandCentres[0].peerID].push(commandCentres[0].unitID);
    mapArray[commandCentres[1].xQuad][commandCentres[1].zQuad][commandCentres[1].peerID].push(commandCentres[1].unitID);
    
    allUnits[commandCentres[0].unitID] = [commandCentres[0].peerID,
        commandCentres[0].unitID,
        0,
        0,
        0,
        commandCentres[0].x,
        commandCentres[0].y,
        commandCentres[0].z,
        "command_centre",
        playerDecks[waveVersion][commandCentres[0].peerID]["command_centre"].HP,
        0,
        0,
        waveVersion,
        [],
        [],
        structureRadius,
        updateStructure,
        "command_centre"];
    
    allUnits[commandCentres[1].unitID] = [commandCentres[1].peerID,
        commandCentres[1].unitID,
        0,
        0,
        0,
        commandCentres[1].x,
        commandCentres[1].y,
        commandCentres[1].z,
        "command_centre",
        playerDecks[waveVersion][commandCentres[1].peerID]["command_centre"].HP,
        0,
        0,
        waveVersion,
        [],
        [],
        structureRadius,
        updateStructure,
        "command_centre"];
    
    
    
}

function isInsideNode(x, y, z, node){
    
    var thisNode = allNodes[node];
    
    if (thisNode){
        return (((x - thisNode.x) * (x - thisNode.x)) + ((z - thisNode.z) * (z - thisNode.z)) <= nodeRadius * nodeRadius)
    } else {
        return false;
    }
    
}

function moveToNode(unitArray){
    
    var data = {
        "xQuotient": undefined,
        "zQuotient": undefined,
        "projectedXHalf": undefined,
        "projectedZHalf": undefined,
        "projectedXHalfQuad": undefined,
        "projectedZHalfQuad": undefined,
        "projectedX": undefined,
        "projectedZ": undefined,
        "projectedXQuad": undefined,
        "projectedZQuad": undefined,
        "theta": undefined,
        "thetaAdjustment": 0, // all theta adjustments are done in 0.5 radian steps
        "x": getUnitX(unitArray),
        "z" : getUnitZ(unitArray),
        "xQuad": ~~(getUnitX(unitArray) * 0.0625),
        "zQuad": ~~(getUnitZ(unitArray) * 0.0625),
        "targetX": allNodes[getUnitNode(unitArray)].x,
        "targetZ": allNodes[getUnitNode(unitArray)].z
    };
    
    //are we at the node? move to next node
    if (isInsideNode(data.x,getUnitY(unitArray), data.z, getUnitNode(unitArray))){
        
        console.log("updating node...");
        
        unitArray[3] = (getUnitNode(unitArray)) + (getUnitDirection(unitArray));
        data.targetX = allNodes[getUnitNode(unitArray)].x;
        data.targetZ = allNodes[getUnitNode(unitArray)].z;
        
    }
    
    var dx = data.targetX - data.x;
    var dz = data.targetZ - data.z;
    
    data.theta = Math.atan2(dx, dz);
    
    console.log("theta: " + data.theta);
    
    moveUnit.updateQuotients(data);
    
    console.log("xQuotient: " + data.xQuotient + " zQuotient: " + data.zQuotient);
    
    // if (radiiAreTouching(data.x, data.z, getUnitMoveSpeed(unitArray), data.targetX, data.targetZ, nodeRadius + 1)){
    //
    //     var scalar = nodeRadius - Math.sqrt((data.x - data.targetX) * (data.x - data.targetX) + (data.z - data.targetZ) * (data.z - data.targetZ));
    //     if (scalar > 0){
    //         console.log("scalar: " + scalar);
    //         moveUnit.scaleQuotientsByScalar(data, scalar);
    //     }
    //
    // } else {
    
    // console.log("using move speed");
    moveUnit.scaleQuotientsByMoveSpeed(data, unitArray);
    // }
    
    // moveUnit.updateHalfProjections(data);
    // moveUnit.detectHalfCollisions(getUnitPeerID(unitArray), data, unitArray);
    
    moveUnit.updateProjections(data);
    moveUnit.detectCollisions(getUnitPeerID(unitArray), data, unitArray);
    
    console.log("xQuotient: " + data.xQuotient + " zQuotient: " + data.zQuotient);
    
    console.log("x: " + data.x + " z: " + data.z);
    
    data.x += data.xQuotient;
    data.z += data.zQuotient;
    
    data.x = (~~(data.x * 1000)) / 1000;
    data.z = (~~(data.z * 1000)) / 1000;
    
    console.log("x: " + data.x + " z: " + data.z);
    
    updateMapPosition(data.x, data.z, data.xQuad, data.zQuad, getUnitPeerID(unitArray), getUnitID(unitArray));
    
    // console.log("oldX: " +getUnitX(unitArray)+ " newX: " + data.x + " oldZ: " + getUnitZ(unitArray)+ " newZ: " + data.z);
    // console.log("distance travelled: " + Math.sqrt((getUnitX(unitArray) - data.x) * (getUnitX(unitArray) - data.x) + (getUnitZ(unitArray) - data.z) * (getUnitZ(unitArray) - data.z)));
    
    unitArray[5] = data.x;
    unitArray[7] = data.z;
    
    return unitArray;
    
}

function isInRange(unitArray) {
    
    var targetArray = allUnits[getUnitTargetID(unitArray)];
    
    var x = getUnitX(unitArray);
    var z = getUnitZ(unitArray);
    
    var targetX = (getUnitX(targetArray));
    var targetZ = (getUnitZ(targetArray));
    var range = getUnitRange(unitArray);
    
    return ((targetX - x) * (targetX - x) + (targetZ - z) * (targetZ - z)) <= (range * range) + (getUnitRadius(targetArray) * getUnitRadius(targetArray));
}

function basicAttack(dataArray){
    
    var targetRef = allUnits[getUnitTargetID(dataArray)];
    
    targetRef[9] -= getUnitDamage(dataArray);
    
    setDidFire(dataArray);
    setReloadDate(dataArray);
    
    console.log("firing basic attack: " + getUnitCode(dataArray));
    
}

// Moves the current unit towards its target.
// Returns a new data array with the updated positions..
function moveToTarget(unitArray){
    
    var data = {
        "xQuotient": undefined,
        "zQuotient": undefined,
        "projectedXHalf": undefined,
        "projectedZHalf": undefined,
        "projectedXHalfQuad": undefined,
        "projectedZHalfQuad": undefined,
        "projectedX": undefined,
        "projectedZ": undefined,
        "projectedXQuad": undefined,
        "projectedZQuad": undefined,
        "theta": undefined,
        "thetaAdjustment": 0, // all theta adjustments are done in 0.5 radian steps
        "x": getUnitX(unitArray),
        "z" : getUnitZ(unitArray),
        "xQuad": ~~(getUnitX(unitArray) * 0.0625),
        "zQuad": ~~(getUnitZ(unitArray) * 0.0625),
        "targetX": allNodes[getUnitNode(unitArray)].x,
        "targetZ": allNodes[getUnitNode(unitArray)].z
    };
    
    var dx = (data.targetX - data.x);
    var dz = (data.targetZ - data.z);
    
    data.theta = Math.atan2(dx, dz);
    
    console.log("theta: " + data.theta);
    
    moveUnit.updateQuotients(data);
    
    console.log("xQuotient: " + data.xQuotient + " zQuotient: " + data.zQuotient);
    
    if (getUnitRange(unitArray) > getUnitMoveSpeed(getUnitMoveSpeed(unitArray))){
        
        var distanceToTravel = (dx * dx + dz * dz) - (getUnitRange(unitArray) * getUnitRange(unitArray) + creepRadius * creepRadius);
        
        if (distanceToTravel > getUnitMoveSpeed(unitArray) * getUnitMoveSpeed(unitArray)){
            
            moveUnit.scaleQuotientsByMoveSpeed(data, unitArray);
            
        } else {
            
            moveUnit.scaleQuotientsByScalar(data, Math.sqrt(distanceToTravel));
            
        }
        
    } else {
        
        if (radiiAreTouching(data.x, data.z, getUnitMoveSpeed(unitArray), data.targetX, data.targetZ, creepRadius)){
            
            var scalar = Math.sqrt((getUnitRange(unitArray) * getUnitRange(unitArray) + creepRadius * creepRadius) - (data.x - data.targetX) * (data.x - data.targetX) + (data.z - data.targetZ) * (data.z - data.targetZ));
            
            moveUnit.scaleQuotientsByScalar(data, scalar);
            
        } else {
            moveUnit.scaleQuotientsByMoveSpeed(data, unitArray);
        }
        
    }
    
    moveUnit.updateProjections(data);
    moveUnit.detectCollisions(getUnitPeerID(unitArray), data, unitArray);
    
    console.log("xQuotient: " + data.xQuotient + " zQuotient: " + data.zQuotient);
    
    data.x += data.xQuotient;
    data.z += data.zQuotient;
    
    data.x = (~~(data.x * 1000)) / 1000;
    data.z = (~~(data.z * 1000)) / 1000;
    
    console.log("x: " + data.x + " z: " + data.z);
    
    updateMapPosition(data.x, data.z, data.xQuad, data.zQuad, getUnitPeerID(unitArray), getUnitID(unitArray));
    
    unitArray[5] = data.x;
    unitArray[7] = data.z;
    
    return unitArray;
    
}

/*
 Checks if the target is still within 1 quad radius or alive.
 Returns boolean.
 */
function isTargetAvailable(dataArray){
    var targetRef = allUnits[getUnitTargetID(dataArray)];
    
    if (!targetRef){
        return false;
    } else if (getUnitHP(targetRef) >= 0){
        
        var targetXQuad = ~~(getUnitX(targetRef) * 0.0625);
        var targetZQuad = ~~(getUnitZ(targetRef) * 0.0625);
        
        var xQuad = ~~(getUnitX(dataArray) * 0.0625);
        var zQuad = ~~(getUnitZ(dataArray) * 0.0625);
        
        var xDistance = (targetXQuad - xQuad) * (targetXQuad - xQuad);
        var zDistance = (targetZQuad - zQuad) * (targetZQuad - zQuad);
        
        return xDistance <= 1 && zDistance <= 1;
        
    } else {
        return false;
    }
    
}

/*
 Checks if the target is within 1 quad radius, alive and not at full HP.
 Returns Boolean.
 */
function isTargetHealable(dataArray){
    
    var target = allUnits[getUnitTargetID(dataArray)];
    
    if (!target){
        return false;
    } else if (getUnitHP(target) < playerDecks[getUnitWaveVersion(target)][getUnitPeerID(target)][getUnitCode(target)].HP && getUnitHP(target) > 0){
        
        var targetXQuad = ~~(getUnitX(target) * 0.0625);
        var targetZQuad = ~~(getUnitZ(target) * 0.0625);
        
        var xQuad = ~~(getUnitX(dataArray) * 0.0625);
        var zQuad = ~~(getUnitZ(dataArray) * 0.0625);
        
        var xDistance = (targetXQuad - xQuad) * (targetXQuad - xQuad);
        var zDistance = (targetZQuad - zQuad) * (targetZQuad - zQuad);
        
        return xDistance <= 1 && zDistance <= 1;
        
    } else {
        return false;
    }
    
}

/*
 Standard search function.
 Returns null or enemy unit ID
 Searches for a unit in +- 1 quad.
 Will target anything on the map in a nearby quadrant.
 */
function searchForTarget(dataArray){
    
    var xQuad = ~~(getUnitX(dataArray) * 0.0625); // binary floor
    var zQuad = ~~(getUnitZ(dataArray) * 0.0625);
    
    var opposingPeerID = getUnitPeerID(dataArray) == 1 ? 2 : 1;
    var creepSightRadius  = getUnitSightRadius(dataArray);
    
    // check if there is more than one peer in our current quad
    if (mapArray[xQuad][zQuad][opposingPeerID][0]){
        
        console.log("returning target");
        return [mapArray[xQuad][zQuad][opposingPeerID][0]];
        
    } else {
        
        for (var i = -creepSightRadius; i <= creepSightRadius; i++){
            
            if (xQuad + i >= 0 && xQuad + i < 32){
                
                for (var j = -creepSightRadius; j <= creepSightRadius; j++){
                    
                    if ((zQuad + j >= 0 && zQuad + j < 32) || (i != 0 && j != 0)){
                        
                        if (mapArray[xQuad + i][zQuad + j][opposingPeerID][0]){
                            console.log("returning target");
                            return [mapArray[xQuad + i][zQuad + j][opposingPeerID][0]];
                        }
                        
                        
                    }
                    
                }
                
            }
        }
        
    }
    return null;
}

/*
 Healer targetting function.
 Returns friendly unit ID or null if no available target found.
 Searches in 1 +- current quad.
 Only accepts non structures, units above 0HP and units with less than full HP.
 NOTE: Does not into treat units with an aura differently. IE Health Aura.
 */
function searchForTargetHealer(dataArray){
    
    console.log("searching for healable target...");
    
    var xQuad = ~~(getUnitX(dataArray) * 0.0625);
    var zQuad = ~~(getUnitZ(dataArray) * 0.0625);
    
    if (mapArray[xQuad][zQuad][getUnitPeerID(dataArray)][0]){
        
        for(var i = 0; i < mapArray[xQuad][zQuad][getUnitPeerID(dataArray)].length; i++){
            
            // console.log("targets available...");
            // console.log(mapArray[xQuad][zQuad][getUnitPeerID(dataArray)]);
            // console.log("xQUAD: " + xQuad + " zQUAD: " + zQuad);
            // console.log("getUnitPeerID: " + getUnitPeerID(dataArray));
            
            var target = allUnits[mapArray[xQuad][zQuad][getUnitPeerID(dataArray)][i]];
            
            if (getUnitHP(target) < playerDecks[getUnitWaveVersion(target)][getUnitPeerID(target)][getUnitCode(target)].HP && getUnitHP(target)> 0 && isNotStructure(target)){ // if target is not full HP, not dead and not structure
                
                // console.log("found target... " + target);
                return [getUnitID(target)];
                
            }
            
        }
        
    }
    
    for (var i = -1; i < 2; i++){
        
        if (xQuad + i >= 0 && xQuad + i < 32){
            
            for (var j = -1; j < 2; j++){
                
                if ((zQuad + j >= 0 && zQuad + j < 32) || (i != 0 && j != 0)){
                    
                    if (mapArray[xQuad + i][zQuad + j][getUnitPeerID(dataArray)][0]){
                        
                        for(var k = 0; k < mapArray[xQuad + i][zQuad + j][getUnitPeerID(dataArray)].length; k++){
                            
                            // console.log("targets available...");
                            // console.log(mapArray[xQuad + i][zQuad + j][getUnitPeerID(dataArray)]);
                            // console.log("xQUAD: " + (xQuad + i) + " zQUAD: " + (zQuad + j));
                            // console.log("getUnitPeerID: " + getUnitPeerID(dataArray));
                            
                            var target = allUnits[mapArray[xQuad + i][zQuad + j][getUnitPeerID(dataArray)][k]];
                            
                            if (getUnitHP(target) < playerDecks[getUnitWaveVersion(target)][getUnitPeerID(target)][getUnitCode(target)].HP && getUnitHP(target)> 0 && isNotStructure(target)){ // if target is not full HP, not dead and not structure
                                
                                // console.log("found target... " + target);
                                return [getUnitID(target)];
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    return null;
    
}

// returns a single target belonging to the passed in peerID.
function scanForTarget(peerID, xQuad, zQuad){
    
    // console.log("scanning for target");
    // console.log("peerID: " + peerID);
    // console.log("xQuad: " + xQuad);
    // console.log("zQuad: " + zQuad);
    
    if (mapArray[xQuad][zQuad][peerID][0]){
        
        // console.log("returning a unit: " + mapArray[xQuad][zQuad][peerID][0]);
        return mapArray[xQuad][zQuad][peerID][0];
        
    } else {
        for (var i = -1; i < 2; i++){
            
            if (xQuad + i >= 0 && xQuad + i < 32){
                
                for (var j = -1; j < 2; j++){
                    
                    if ((zQuad + j >= 0 && zQuad + j < 32) || (i != 0 && j != 0)){
                        
                        if (mapArray[xQuad + i][zQuad + j][peerID][0]){
                            // console.log("returning a unit: " + mapArray[xQuad][zQuad][peerID][0]);
                            return mapArray[xQuad + i][zQuad + j][peerID][0];
                        }
                        
                    }
                    
                }
                
            }
        }
    }
    return null;
}

function scanForChainedTargets(peerID, xQuad, zQuad, chainRadius, chainLength){
    
    var targets = [];
    var initTarget = scanForTarget(peerID, xQuad, zQuad);
    
    if (!initTarget){
        
        return null;
        
    } else {
        
        targets.push(initTarget);
        
    }
    
    for (var k = 0; k < chainLength; k++){
        
        var targetX = allUnits[targets[targets.length - 1]][5];
        var targetZ = allUnits[targets[targets.length - 1]][7];
        
        var currentTargetLength = targets.length;
        
        // var targetXQuad = ~~(targetX * 0.0625);
        // var targetZQuad = ~~(targetZ * 0.0625);
        
        var minXQuad = ~~((targetX - chainRadius) * 0.0625);
        var maxXQuad = ~~((targetX + chainRadius) * 0.0625);
        
        var minZQuad = ~~((targetX - chainRadius) * 0.0625);
        var maxZQuad = ~~((targetX + chainRadius) * 0.0625);
        
        for (var i = minXQuad; i < maxXQuad && currentTargetLength === targets.length; i++){
            
            if (i >= 0 && i < 32){
                
                for (var j = minZQuad; j < maxZQuad && currentTargetLength === targets.length; j++){
                    
                    if (j >= 0 && j < 32){
                        
                        for (var h = 0; h < mapArray[i][j][peerID].length; h++){
                            var unitID = mapArray[i][j][peerID][h];
                            var x = allUnits[unitID][5];
                            var z = allUnits[unitID][7];
                            
                            if (unitID !== targets[targets.length - 1] && (x - targetX) * (x - targetX) + (x - targetX) * (x - targetX) <= chainRadius){
                                targets.push(unitID);
                                break;
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
    }
    
    
}

// returns n targets belonging to the passed in peerID;
function scanForTargets(peerID, xQuad, zQuad, n){
    
    var targets = [];
    
    if (mapArray[xQuad][zQuad][peerID][0]){
        
        for (var i = 0; i < mapArray[xQuad][zQuad][peerID].length && targets.length < n; i++){
            targets.push(mapArray[xQuad][zQuad][peerID][i]);
        }
        
    }
    
    if (targets.length < n){
        
        for (var i = -1; i < 2 && targets.length < n; i++){
            
            if (xQuad + i >= 0 && xQuad + i < 32){
                
                for (var j = -1; j < 2 && targets.length < n; j++){
                    
                    if ((i != 0 && j != 0) && (zQuad + j >= 0 && zQuad + j < 32)){
                        
                        if (mapArray[xQuad + i][zQuad + j][peerID][0]){
                            
                            for (var k = 0; k < mapArray[xQuad][zQuad][peerID].length && targets.length < n; k++){
                                targets.push(mapArray[xQuad][zQuad][peerID][k]);
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
            }
        }
        
    }
    
    return targets;
    
}

function scanForTargetsInQuadRadius(xQuad, zQuad, peerID, radius){
    
    var targets = [];
    
    for (var i = -radius; i <= radius; i++){
        
        if ((xQuad + i) >= 0 && (xQuad + i) < 32){
            
            // console.log("xQuad: " + (xQuad + i));
            
            for (var j = -radius; j <= radius; j++){
                
                if ((zQuad + j) >= 0 && (zQuad + j) < 32){
                    
                    console.log("zQuad: " + (zQuad + j));
                    
                    for(var k = 0; k < mapArray[(xQuad + i)][(zQuad + j)][peerID].length; k++){
                        
                        targets.push(mapArray[(xQuad + i)][(zQuad + j)][peerID][k]);
                        console.log("found target");
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    console.log("targets: " + targets);
    return targets;
    
}

/*
 A units sight is determined in quadrants.
 It is calculated by taking the ceiling of unitRange / creepRadius
 + 1.
 */
function getUnitSightRadius(dataArray){
    
    return Math.ceil(getUnitRange(dataArray) / creepRadius) + 1;
    
}

/*
 Gets all units in a given radius.
 returns array of unit IDS.
 will expand search to neighbouring quads if required.
 */
function getAllUnitsInRadius(peerID, x, z, radius){
    
    var minXQuad = ~~((x - radius) * 0.0625);
    var maxXQuad = ~~((x + radius) * 0.0625);
    
    var minZQuad = ~~((z - radius) * 0.0625);
    var maxZQuad = ~~((z + radius) * 0.0625);
    
    var units = [];
    
    
    
    for (var i = minXQuad; i <= maxXQuad; i++){
        
        if (i >= 0 && i < 32){
            
            for (var j = minZQuad; j <= maxZQuad; j++){
                
                if (j >= 0 && j < 32){
                    
                    mapArray[i][j][peerID].forEach(function (unitID) {
                        
                        if (isInsidePoint(x, z, getUnitX(allUnits[unitID]), getUnitZ(allUnits[unitID]), radius)){
                            
                            units.push(unitID);
                            
                        }
                        
                    });
                    
                }
                
            }
            
        }
        
    }
    
    console.log("units: " + units);
    
    return units;
}

function getChainHealTargets(unitArray) {
    var targetPeerID = getUnitPeerID(unitArray);
    var targets = [getUnitTargetID(unitArray)];
    var chainLength = getUnitHitLimit(unitArray);
    var chainRadius = getUnitAOERadius(unitArray);
    
    console.log("peerID: " + targetPeerID);
    console.log("targets: " + targets);
    console.log("chainLength: " + chainLength);
    console.log("chainRadius: " + chainRadius);
    
    for (var k = 0; k < chainLength - 1; k++){
        
        var lastTarget = allUnits[targets[targets.length - 1]];
        var currentTargetsLength = targets.length;
        
        var minXQuad = ~~((getUnitX(lastTarget) - chainRadius) * 0.0625);
        var maxXQuad = ~~((getUnitX(lastTarget) + chainRadius) * 0.0625);
        
        var minZQuad = ~~((getUnitZ(lastTarget) - chainRadius) * 0.0625);
        var maxZQuad = ~~((getUnitZ(lastTarget) + chainRadius) * 0.0625);
        
        console.log("k loop");
        
        for (var i = minXQuad; i <= maxXQuad && currentTargetsLength === targets.length; i++){
            
            // console.log("i loop");
            
            if (i >= 0 && i < 32){
                
                for (var j = minZQuad; j <= maxZQuad && currentTargetsLength === targets.length; j++){
                    
                    // console.log("xQuad: " + i + " zQuad: " + j);
                    
                    // console.log("j loop");
                    
                    if (j >= 0 && j < 32){
                        
                        for (var h = 0; h < mapArray[i][j][targetPeerID].length; h++){
                            
                            // console.log("h loop");
                            
                            var unitIDFound = mapArray[i][j][targetPeerID][h];
                            var unitArrayFound = allUnits[unitIDFound];
                            
                            if (unitIDFound !== getUnitID(lastTarget) &&
                                getUnitHP(unitArrayFound) > 0 &&
                                getUnitHP(unitArrayFound) < playerDecks[getUnitWaveVersion(unitArrayFound)][getUnitPeerID(unitArrayFound)][getUnitCode(unitArrayFound)].HP &&
                                isInsidePoint(getUnitX(lastTarget), getUnitZ(lastTarget), getUnitX(allUnits[unitIDFound]), getUnitZ(allUnits[unitIDFound]), chainRadius)){
                                
                                targets.push(unitIDFound);
                                break;
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    return targets;
}

function getChainableTargets(unitArray){
    
    var targetPeerID = getUnitPeerID(allUnits[getUnitTargetID(unitArray)]);
    var targets = [getUnitTargetID(unitArray)];
    var chainLength = getUnitHitLimit(unitArray);
    var chainRadius = getUnitAOERadius(unitArray);
    
    for (var k = 0; k < chainLength - 1; k++){
        
        var lastTarget = allUnits[targets[targets.length - 1]];
        var currentTargetsLength = targets.length;
        
        var minXQuad = ~~((getUnitX(lastTarget) - chainRadius) * 0.0625);
        var maxXQuad = ~~((getUnitX(lastTarget) + chainRadius) * 0.0625);
        
        var minZQuad = ~~((getUnitZ(lastTarget) - chainRadius) * 0.0625);
        var maxZQuad = ~~((getUnitZ(lastTarget) + chainRadius) * 0.0625);
        
        for (var i = minXQuad; i <= maxXQuad && currentTargetsLength === targets.length; i++){
            
            if (i >= 0 && i < 32){
                
                for (var j = minZQuad; j <= maxZQuad && currentTargetsLength === targets.length; j++){
                    
                    
                    if (j >= 0 && j < 32){
                        
                        for (var h = 0; h < mapArray[i][j][targetPeerID].length; h++){
                            
                            
                            var unitIDFound = mapArray[i][j][targetPeerID][h];
                            
                            if (unitIDFound !== getUnitID(lastTarget) && isInsidePoint(getUnitX(lastTarget), getUnitZ(lastTarget), getUnitX(allUnits[unitIDFound]), getUnitZ(allUnits[unitIDFound]), chainRadius)){
                                
                                targets.push(unitIDFound);
                                break;
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    return targets;
    
}

// Must be called at some point in the initialise match process.
// Takes in a players inventory scriptData and sets up a deck object.
// The deck is a summary of each players units / defenses..
function initialisePlayerDeck(peerID){
    
    var unitsRAW = {}; unitsRAW = playerInventory[peerID].units;
    var unitKeys = Object.keys(unitsRAW);
    
    var structureRAW = {}; structureRAW = playerInventory[peerID].defenses;
    var structureKeys = Object.keys(structureRAW);
    
    for (var i = 0; i < unitKeys.length; i++){
        
        if (unitsRAW[unitKeys[i]].level != 0){
            
            var RAWUnitData = unitsRAW[unitKeys[i]];
            
            var weaponBaseStats = getItemStats(playerInventory[peerID].weapons[RAWUnitData.weapon]); // weapon buffs
            var armourBaseStats = getItemStats(playerInventory[peerID].armour[RAWUnitData.armour]); // armour buffs
            var unitBaseStats = getUnitStats(unitKeys[i], RAWUnitData); // unit base stats
            
            playerDecks[waveVersion][peerID][unitKeys[i]] = {
                "GOLD": weaponBaseStats.GOLD + armourBaseStats.GOLD + unitBaseStats.GOLD,
                "LUMBER": weaponBaseStats.LUMBER + armourBaseStats.LUMBER + unitBaseStats.LUMBER,
                "HP": weaponBaseStats.HP + armourBaseStats.HP + unitBaseStats.HP,
                "RANGE": weaponBaseStats.RANGE + armourBaseStats.RANGE + unitBaseStats.RANGE,
                "MOVEMENT_SPEED": weaponBaseStats.MOVEMENT_SPEED + armourBaseStats.MOVEMENT_SPEED + unitBaseStats.MOVEMENT_SPEED,
                "ATTACK_SPEED": weaponBaseStats.ATTACK_SPEED + armourBaseStats.ATTACK_SPEED + unitBaseStats.ATTACK_SPEED,
                "HIT_LIMIT": weaponBaseStats.HIT_LIMIT + armourBaseStats.HIT_LIMIT + unitBaseStats.HIT_LIMIT,
                "AOE_RADIUS": weaponBaseStats.AOE_RADIUS + armourBaseStats.AOE_RADIUS + unitBaseStats.AOE_RADIUS,
                "DAMAGE": weaponBaseStats.DAMAGE + armourBaseStats.DAMAGE + unitBaseStats.DAMAGE
            };
            
        }
        
    }
    
    for(var i = 0; i < structureKeys.length; i++){
        
        if (structureRAW[structureKeys[i]].level != 0){
            
            var RAWStructureData = structureRAW[structureKeys[i]];
            
            var weaponBaseStats = {};
            
            if (RAWStructureData.weapon){
                
                weaponBaseStats = getItemStats(RAWStructureData.weapon);
                
            } else {
                
                weaponBaseStats = {
                    "GOLD": 0,
                    "LUMBER": 0,
                    "DAMAGE": 0,
                    "HP": 0,
                    "RANGE": 0,
                    "MOVEMENT_SPEED": 0,
                    "ATTACK_SPEED": 0,
                    "HIT_LIMIT": 0,
                    "AOE_RADIUS": 0,
                    "CONSTRUCTION_TIME": 0
                };
                
            }
            
            var armourBaseStats = getItemStats(RAWStructureData.armour);
            var unitBaseStats = getStructureStats(structureKeys[i], RAWStructureData);
            
            playerDecks[waveVersion][peerID][structureKeys[i]] = {
                "GOLD": weaponBaseStats.GOLD + armourBaseStats.GOLD + unitBaseStats.GOLD,
                "LUMBER": weaponBaseStats.LUMBER + armourBaseStats.LUMBER + unitBaseStats.LUMBER,
                "HP": weaponBaseStats.HP + armourBaseStats.HP + unitBaseStats.HP,
                "RANGE": weaponBaseStats.RANGE + armourBaseStats.RANGE + unitBaseStats.RANGE,
                "MOVEMENT_SPEED": weaponBaseStats.MOVEMENT_SPEED + armourBaseStats.MOVEMENT_SPEED + unitBaseStats.MOVEMENT_SPEED,
                "ATTACK_SPEED": weaponBaseStats.ATTACK_SPEED + armourBaseStats.ATTACK_SPEED + unitBaseStats.ATTACK_SPEED,
                "HIT_LIMIT": weaponBaseStats.HIT_LIMIT + armourBaseStats.HIT_LIMIT + unitBaseStats.HIT_LIMIT,
                "AOE_RADIUS": weaponBaseStats.AOE_RADIUS + armourBaseStats.AOE_RADIUS + unitBaseStats.AOE_RADIUS,
                "DAMAGE": weaponBaseStats.DAMAGE + armourBaseStats.DAMAGE + unitBaseStats.DAMAGE,
                "CONSTRUCTION_TIME":  unitBaseStats.CONSTRUCTION_TIME + armourBaseStats.CONSTRUCTION_TIME + weaponBaseStats.CONSTRUCTION_TIME,
                "ABILITY": unitBaseStats.ABILITY
            };
            
        }
        
    }
    
}


// Returns the relevant statistics for an item.
// IE resource cost, damage and speed buffs...
function getItemStats(itemData){
    return {
        "GOLD": 10,
        "LUMBER": 10,
        "DAMAGE": 5,
        "HP": 0,
        "RANGE": 0,
        "MOVEMENT_SPEED": 0,
        "ATTACK_SPEED": 0,
        "HIT_LIMIT": 1,
        "AOE_RADIUS": 1,
        "CONSTRUCTION_TIME": 0
    };
}

function getUnitStats(name, unitData){
    
    var obj = {
        "GOLD": 50,
        "LUMBER": 15,
        "DAMAGE": 10,
        "HP": 100,
        "RANGE": 2.5,
        "MOVEMENT_SPEED": 4,
        "ATTACK_SPEED": 1500,
        "HIT_LIMIT": 3,
        "AOE_RADIUS": 1,
    };
    
    switch (name) {
        case "healer":
            obj.DAMAGE = -30;
            break;
        case "archer":
            obj.RANGE = 30;
            break;
        case "wizard":
            obj.RANGE = 20;
            break;
        case "catapult":
            obj.RANGE = 30;
        default:
            break;
    }
    
    return obj;
}

function getStructureStats(name, structureData){
    
    var obj = {
        "GOLD": 50,
        "LUMBER": 15,
        "DAMAGE": 10,
        "HP": 100,
        "RANGE": 1.5,
        "MOVEMENT_SPEED": 0,
        "ATTACK_SPEED": 1500,
        "HIT_LIMIT": 1,
        "AOE_RADIUS": 1,
        "CONSTRUCTION_TIME": 40
    };
    
    switch (name) {
        case "gold_mine":
            obj.ABILITY = gainGold;
            break;
        case "tower":
            obj.ABILITY = singleShot;
            break;
        case "command_centre":
            obj.ABILITY = gainResources;
            break;
        case "lumber_mill":
            obj.ABILITY = gainLumber;
            break;
        case "road":
            break;
        case "fort":
            obj.ABILITY = multiShot;
            break;
        case "explosive_trap":
            obj.ABILITY = AOEShot;
            break;
    }
    
    return obj;
}

/*
 ------------------------------------
 UNIT ABILITIES
 ------------------------------------
 */

function gainResources(structureArray){
    playerResources[getUnitPeerID(structureArray)].GOLD += GoldPerUpdateCycle * 0.5;
    playerResources[getUnitPeerID(structureArray)].LUMBER += LumberPerUpdateCycle * 0.5;
}

function gainGold(structureArray){
    playerResources[getUnitPeerID(structureArray)].GOLD += GoldPerUpdateCycle;
    // console.log("gaining gold...");
}

function gainLumber(structureArray){
    playerResources[getUnitPeerID(structureArray)].LUMBER += LumberPerUpdateCycle;
    // console.log("gaining lumber...");
}

function singleShot(structureArray){
    
    if (notReloading(structureArray)){
        
        var opposingPeerID = structureArray[0] == 1 ? 2 : 1;
        var targetID = scanForTarget(opposingPeerID, buildSites[structureArray[3]].xQuad, buildSites[structureArray[3]].zQuad);
        
        if (targetID){
            
            structureArray[11] = Date.now() + playerDecks[waveVersion][structureArray[0]][structureArray[8]].ATTACK_SPEED;
            structureArray[10] = 1;
            allUnits[targetID][9] -= playerDecks[waveVersion][structureArray[0]][structureArray[8]].DAMAGE;
            console.log("firing single shot...");
            
        }
        
    }
    
}

function AOEShot(structureArray) {
    
    if (notReloading(structureArray)) {
        
        var targets = scanForTargetsInQuadRadius(getStructureXQuad(structureArray), getStructureZQuad(structureArray), getOpposingPeerID(structureArray), 1);
        
        if (targets.length > 0){
            setTimeout(explodeTrap, gameStateUpdateRate * trapTurnsToExplosion - 50, structureArray);
            setReloadDate(structureArray);
        }
        
        function explodeTrap(structureArray) {
            
            if (targets.length > 0) {
                
                targets.forEach(function (targetID) {
                    
                    allUnits[targetID][9] -= playerDecks[getUnitWaveVersion(structureArray)][getUnitPeerID(structureArray)][getUnitCode(structureArray)].DAMAGE;
                    
                });
                
                setDidFire(structureArray);
                
                console.log("firing aoe shot...");
                structureArray[9] = 0;
                
            }
        }
    }
}

function chainShot(structureArray){
    if (notReloading(structureArray)){
        
        var opposingPeerID = getOpposingPeerID(structureArray);
        var xQuad = getStructureXQuad(structureArray);
        var zQuad = getStructureZQuad(structureArray);
        var peerID = structureArray[0];
        var structureCode = structureArray[8];
        
        var targets = scanForTargets(opposingPeerID, xQuad, zQuad, playerDecks[waveVersion][peerID][structureCode].HIT_LIMIT);
        
        if (targets.length > 0){
            
            var damage = playerDecks[waveVersion][peerID][structureCode].DAMAGE;
            
            targets.forEach(function(targetID){
                
                allUnits[targetID][9] -= ~~(damage * 0.8);
                
            });
            
            structureArray[10] = 1;
            structureArray[11] = Date.now() + playerDecks[waveVersion][peerID][structureCode].ATTACK_SPEED;
            
        }
        
    }
}

function multiShot(structureArray){
    
    if (notReloading(structureArray)){
        
        var opposingPeerID = getOpposingPeerID(structureArray);
        var xQuad = getStructureXQuad(structureArray);
        var zQuad = getStructureZQuad(structureArray);
        var peerID = getUnitPeerID(structureArray);
        var structureCode = getUnitCode(structureArray);
        
        var targets = scanForTargets(opposingPeerID, xQuad, zQuad, playerDecks[getUnitWaveVersion(structureArray)][peerID][structureCode].HIT_LIMIT);
        
        if (targets.length > 0){
            
            targets.forEach(function(targetID){
                
                allUnits[targetID][9] -= playerDecks[waveVersion][peerID][structureCode].DAMAGE;
                
            });
            
            setDidFire(structureArray);
            setReloadDate(structureArray);
            
            console.log("firing multi shot");
            
        }
        
    }
    
}

function fireBolt(unitArray){
    
    var target = allUnits[getUnitTargetID(unitArray)];
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(target), getUnitX(target), getUnitZ(target), getUnitAOERadius(unitArray));
    
    aoeTargets.forEach(function (unitID) {
        allUnits[unitID][9] -= getUnitDamage(unitArray);
    });
    
    setUnitTarget(aoeTargets, unitArray);
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("firing fire bolt");
}

function iceShard(unitArray){
    
    var target = allUnits[getUnitTargetID(unitArray)];
    
    console.log("no buff movement speed: " + allUnits[getUnitTargetID(unitArray)][21]);
    
    target[9] -= getUnitDamage(unitArray);
    target[21] *= 0.5;
    pushDebuff("cold", allUnits[getUnitTargetID(unitArray)]);
    
    setTimeout(removeIceShardDebuff, 1000, target);
    
    console.log("debuffed movement speed: " + allUnits[target]);
    
    setDidFire(unitArray);
    setReloadDate(unitArray);
    console.log("firing ice shard");
    
}

function chainLightning(unitArray){
    
    var damage = getUnitDamage(unitArray);
    var chainedTargets = getChainableTargets(unitArray);
    
    chainedTargets.forEach(function (unitID) {
        
        allUnits[unitID][9] -= damage;
        damage *= 0.8;
        
    });
    
    
    setUnitTarget(chainedTargets, unitArray);
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("firing chain lightning");
    console.log("targets: " + chainedTargets);
}

function regularShot(unitArray){
    
    var damage = getUnitDamage(unitArray);
    var targetID = getUnitTargetID(unitArray);
    var targetArray = allUnits[targetID];
    
    if (isNotStructure(targetArray)){
        targetArray[9] -= damage;
    } else {
        targetArray[9] -= (damage * 2);
    }
    
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("firing regular shot");
    
}

function multiShot_Catapult(unitArray){
    
    var damage = getUnitDamage(unitArray);
    var targetID = getUnitTargetID(unitArray);
    var targetArray = allUnits[targetID];
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(targetArray), getUnitX(targetArray), getUnitZ(targetArray), getUnitAOERadius(targetArray));
    
    aoeTargets.forEach(function (unitID) {
        
        var unitRef = allUnits[unitID];
        
        if (isNotStructure(unitRef)){
            unitRef[9] -= damage;
        } else {
            unitRef[9] -= (damage * 0.25);
        }
        
    });
    
    setUnitTarget(aoeTargets, unitArray);
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("firing multishot catapult");
    
}

function flameShot(unitArray){
    
    var damage = getUnitDamage(unitArray);
    var targetID = getUnitTargetID(unitArray);
    var targetArray = allUnits[targetID];
    var tickCount = getUnitHitLimit(unitArray);
    
    if (isNotStructure(targetArray)){
        targetArray[9] -= damage;
    } else {
        targetArray[9] -= (damage * 1.5);
    }
    
    if (Math.random() > 0.5){
        
        pushDebuff("burning", targetID);
        
        setTimeout(removeDebuff, (gameStateUpdateRate * tickCount) - (gameStateUpdateRate * 0.1), "burning", targetID);
        
        for (var i = 1; i <= tickCount; i++){
            
            setTimeout(damageOverTime, (gameStateUpdateRate * i) - (gameStateUpdateRate * 0.1), targetID, damage);
            
        }
        
    }
    
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("firing flame shot");
    console.log(targetArray);
}

function chainHeal(unitArray){
    
    var chainedTargets = getChainHealTargets(unitArray);
    
    // console.log(chainedTargets);
    
    // setUnitTarget(unitArray, chainedTargets);
    
    console.log(chainedTargets);
    
    chainedTargets.forEach(function (targetID) {
        
        allUnits[targetID][9] -= getUnitDamage(unitArray);
        
    });
    
    setUnitTarget(chainedTargets, unitArray);
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("chain healing");
    console.log("targets: " + chainedTargets);
    
}

function lifeSteal(unitArray){
    
    var targetArray = allUnits[getUnitTargetID(unitArray)];
    
    targetArray[9] -= getUnitDamage(unitArray);
    unitArray[9] += (getUnitDamage(unitArray) * 0.3);
    
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    
    console.log("life stealing");
}

function convert(unitArray){
    
    var targetArray = allUnits[getUnitTargetID(unitArray)];
    
    targetArray[9] -= getUnitDamage(unitArray);
    
    if (targetArray[9] <= 0){
        
        var didConvert = Math.random();
        
        if (didConvert < 0.3){
            
            var updateFunction = getUpdateFunction(getUnitCode(unitArray));
            var targetFunction = getTargetFunction(getUnitCode(unitArray));
            var attackFunction = (getUnitPeerID(unitArray), getUnitCode(unitArray));
            var targetCheckerFunction = getTargetCheckerFunction(getUnitCode(unitArray));
            var unitSummary = playerDecks[getUnitWaveVersion(unitArray)][getUnitPeerID(unitArray)][getUnitCode(unitArray)];
            
            allUnits[unitIDCounter] = [getUnitPeerID(unitArray),
                (unitIDCounter++),
                getUnitDirection(unitArray),
                getUnitNode(unitArray),
                0,
                getUnitX(targetArray),
                getUnitY(targetArray),
                getUnitZ(targetArray),
                getUnitCode(unitArray),
                unitSummary.HP,
                0,
                0,
                getUnitWaveVersion(unitArray),
                ["converted"],
                [],
                updateFunction,
                targetFunction,
                attackFunction,
                targetCheckerFunction,
                unitSummary.RANGE,
                unitSummary.MOVEMENT_SPEED,
                unitSummary.ATTACK_SPEED,
                unitSummary.DAMAGE,
                unitSummary.AOE_RADIUS,
                unitSummary.HIT_LIMIT];
            
        }
        
    }
    
    setDidFire(unitArray);
    setReloadDate(unitArray);
    
    console.log("attempting to convert..");
}

function healthAura(unitArray){
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(unitArray), getUnitX(unitArray), getUnitZ(unitArray), getUnitAOERadius(unitArray));
    
    aoeTargets.forEach(function (targetID) {
        
        if (allUnits[targetID][13].indexOf("healthAura") === -1){
            
            allUnits[targetID][13].push("healthAura");
            allUnits[targetID][9] *= 1.25;
            
            setTimeout(function () {
                
                allUnits[targetID][13].splice(allUnits[targetID][13].indexOf("healthAura"));
                allUnits[targetID][9] *= 0.8;
                
            }, 2 * gameStateUpdateRate);
        }
        
    });
    
    setUnitTarget(aoeTargets);
    console.log("firing health aura");
    
}

function speedAura(unitArray){
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(unitArray), getUnitX(unitArray), getUnitZ(unitArray), getUnitAOERadius(unitArray));
    
    aoeTargets.forEach(function (targetID) {
        
        if (allUnits[targetID][13].indexOf("speedAura") === -1){
            
            allUnits[targetID][13].push("speedAura");
            allUnits[targetID][21] *= 1.25;
            allUnits[targetID][22] *= 1.25;
            
            setTimeout(function () {
                
                allUnits[targetID][13].splice(allUnits[targetID][13].indexOf("speedAura"));
                allUnits[targetID][21] *= 0.8;
                allUnits[targetID][22] *= 0.8;
                
            }, 2 * gameStateUpdateRate);
        }
        
    });
    
    setUnitTarget(aoeTargets);
    console.log("firing speed aura");
    
}

function damageAura(unitArray){
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(unitArray), getUnitX(unitArray), getUnitZ(unitArray), getUnitAOERadius(unitArray));
    
    aoeTargets.forEach(function (targetID) {
        
        if (allUnits[targetID][13].indexOf("damageAura") === -1){
            
            allUnits[targetID][13].push("damageAura");
            allUnits[targetID][23] *= 1.25;
            
            setTimeout(function () {
                
                allUnits[targetID][13].splice(allUnits[targetID][13].indexOf("damageAura"));
                allUnits[targetID][23] *= 0.8;
                
            }, 2 * gameStateUpdateRate);
        }
        
    });
    
    setUnitTarget(aoeTargets);
    
    console.log("firing damage aura");
    
}

function regenAura(unitArray){
    
    var aoeTargets = getAllUnitsInRadius(getUnitPeerID(unitArray), getUnitX(unitArray), getUnitZ(unitArray), getUnitAOERadius(unitArray));
    
    aoeTargets.forEach(function (targetID) {
        
        var targetArray = allUnits[targetID];
        
        if (targetArray[9] < playerDecks[getUnitWaveVersion(targetArray)][getUnitPeerID(targetArray)][getUnitCode(targetArray)].HP){
            
            if (targetArray[13].indexOf("regenAura") === -1){
                
                targetArray[13].push("regenAura");
                
                setTimeout(function () {
                    
                    targetArray[13].splice(targetArray[13].indexOf("regenAura"));
                    
                }, 2 * gameStateUpdateRate);
                
            }
            
            var hpDiff = (playerDecks[getUnitWaveVersion(targetArray)][getUnitPeerID(targetArray)][getUnitCode(targetArray)].HP - targetArray[9]);
            var hpRegen = playerDecks[getUnitWaveVersion(targetArray)][getUnitPeerID(targetArray)][getUnitCode(targetArray)].HP * 0.025;
            
            targetArray[9] += hpDiff < hpRegen ? hpDiff : hpRegen;
            
        }
        
    });
    
    setUnitTarget(aoeTargets, unitArray);
    
    console.log("firing regen aura");
    
}

/*
 --------------------------------
 CONVENIENCE
 --------------------------------
 */

function getStructureXQuad(structureArray){
    return buildSites[structureArray[3]].xQuad;
}

function getStructureZQuad(structureArray){
    return buildSites[structureArray[3]].zQuad;
}

function getOpposingPeerID(dataArray){
    return dataArray[0] == 1 ? 2 : 1;
}

function notReloading(unitArray){
    return unitArray[11] <= Date.now();
}

function getUnitAOERadius(unitArray){
    return unitArray[24];
}

function getUnitMoveSpeed(unitArray) {
    return unitArray[21];
}

function getUnitID(unitArray) {
    return unitArray[1];
}

function getUnitPeerID(unitArray){
    return unitArray[0];
}

function getUnitX(unitArray) {
    return unitArray[5];
}

function getUnitY(unitArray) {
    return unitArray[6];
}

function getUnitZ(unitArray) {
    return unitArray[7];
}

function setUnitTarget(targetID, unitArray) {
    unitArray[4] = targetID;
}

function getUnitRange(unitArray) {
    return unitArray[21];
}

function getUnitCode(unitArray) {
    return unitArray[8];
}

function getHardStructureCode(unitArray){
    return unitArray[17];
}

function getUnitHP(unitArray) {
    return unitArray[9];
}

function getUnitWaveVersion(unitArray) {
    return unitArray[12];
}

function getUnitTargetID(unitArray) {
    return unitArray[4][0];
}

function getUnitDirection(unitArray) {
    return unitArray[2];
}

function getUnitNode(unitArray) {
    return unitArray[3];
}

function getUnitDamage(unitArray) {
    return unitArray[23];
}

function getUnitRadius(unitArray) {
    return unitArray[15];
}

function getUnitAttackSpeed(unitArray){
    return unitArray[22];
}

function getUnitHitLimit(unitArray) {
    return unitArray[25];
}

function setReloadDate(unitArray){
    var attackSpeed = getUnitAttackSpeed(unitArray);
    
    unitArray[11] = Date.now() + (attackSpeed ? attackSpeed : playerDecks[getUnitWaveVersion(unitArray)][getUnitPeerID(unitArray)][getUnitCode(unitArray)].ATTACK_SPEED);
    
    console.log(getUnitCode(unitArray) + " : reload date " + unitArray[11]);
}

function setDidFire(unitArray) {
    unitArray[10] = 1;
}

function pushBuff(buff, unitArray){
    unitArray[13].push(buff);
}

function pushDebuff(debuff, unitID){
    console.log("pushing debuff... " + debuff);
    if (allUnits[unitID]){
        allUnits[unitID][14].push(debuff);
    }
    
}

function removeBuff(buff, unitArray){
    unitArray[13].splice(unitArray.indexOf(buff), 1);
}

function removeDebuff(debuff, unitID){
    console.log("removing debuff... " + debuff);
    if (allUnits[unitID]){
        allUnits[unitID][14].splice(allUnits[unitID][14].indexOf(debuff), 1);
    }
    
    console.log(allUnits[unitID]);
}

function updateMapPosition(x, z, xQuad, zQuad, peerID, unitID) {
    var newXQuad = ~~(x * 0.0625);
    var newZQuad = ~~(z * 0.0625);
    
    if (xQuad != newXQuad || zQuad != newZQuad){
        
        mapArray[xQuad][zQuad][peerID].splice(mapArray[xQuad][zQuad][peerID].indexOf(unitID), 1);
        mapArray[newXQuad][newZQuad][peerID].push(unitID);
        
        console.log("unit " +unitID+ " moved to quad: " + newXQuad + " : " + newZQuad);
    }
}

function damageOverTime(unitID, damage) {
    if (allUnits[unitID]){
        allUnits[unitID][9] -= (damage * 0.2);
    }
    
    console.log("logging unit HP");
    console.log(allUnits[unitID]);
}

function removeIceShardDebuff(targetID){
    if (allUnits[targetID]){
        allUnits[targetID][21] *= 2;
        removeDebuff("cold", targetID);
        console.log("removed debuff movement speed: " + allUnits[targetID]);
    }
}

function executeAttack(unitArray){
    unitArray[18](unitArray);
}

function executeTargetSearcher(unitArray){
    return unitArray[17](unitArray);
}

function executeTargetChecker(unitArray){
    return unitArray[19](unitArray);
}

function isNotStructure(array){
    switch(array[8]){
        case "warrior":
            return true;
        case "archer":
            return true;
        case "catapult":
            return true;
        case "wizard":
            return true;
        case "cavalry":
            return true;
        case "monk":
            return true;
        case "healer":
            return true;
        case "zombie":
            return true;
        default:
            return false;
    }
}

function getAttackFunction(peerID, unitCode){
    
    var weapon = playerInventory[peerID].units[unitCode].weapon;
    
    switch (weapon){
        case "fire_bolt": // small AOE
            return fireBolt;
        case "ice_shard": // temporarily slows enemy move speed
            return iceShard;
        case "chain_lightning": // chains to nearby enemies
            return chainLightning;
        case "regular_shot": // does more damage to structures
            return regularShot;
        case "multi_shot": // does more damage to units in small aoe. does slightly more damage to structures
            return multiShot_Catapult;
        case "flame_shot": // does lots of damage to structures
            return flameShot;
        case "chain_heal": // chain heal to nearby allies
            return chainHeal;
        case "life_steal": // steals HP from enemy and gives to zombie
            return lifeSteal;
        case "convert": // if zombie lands killing blow has % to convert enemy unit to zombie
            return convert;
        case "health_aura": // increases hp of nearby units
            return healthAura;
        case "speed_aura": // increases nearby units move speed
            return speedAura;
        case "damage_aura": // increases nearby units damage
            return damageAura;
        case "regen_aura": // causes nearby units to reflect damage
            return regenAura;
        default:
            return basicAttack;
        
    }
    
}

function getTargetFunction(unitCode){
    
    switch (unitCode){
        case "healer":
            return searchForTargetHealer;
        default:
            return searchForTarget;
    }
    
}

function getUpdateFunction(unitCode){
    
    switch (unitCode){
        case "monk":
            return updateMonk;
        default:
            return updateUnit;
    }
    
}

function getTargetCheckerFunction(unitCode){
    
    switch (unitCode){
        case "healer":
            return isTargetHealable;
        default:
            return isTargetAvailable;
    }
    
}

function isInsidePoint(pointX, pointZ, x, z, radius){
    return (x - pointX) * (x - pointX) + (z - pointZ) * (z - pointZ) <= radius * radius;
}

function radiiAreTouching(x1, z1, r1, x2, z2, r2) {
    return (x1 - x2) * (x1 - x2) + (z1 - z2) * (z1 - z2) <= (r1 * r1) + (r2 * r2);
}

function isTargetingWaypoint(unitArray) {
    return unitArray[2] !== 0;
}

function setTargetTypeToCreep(unitArray) {
    unitArray[2] = 0;
}

function setTargetTypeToWaypoint(unitArray) {
    unitArray[2] = getUnitPeerID(unitArray) == 1 ? -1 : 1;
}

var moveUnit = {
    
    // updates the quotients using theta
    updateQuotients: function (data) {
        // console.log(data.theta);
        
        data.xQuotient = Math.sin(data.theta);
        data.zQuotient = Math.cos(data.theta);
        
        // data.xQuotient = Math.sin(data.theta) * Math.sin(data.theta) - 1;
        // data.zQuotient = Math.cos(data.theta) * Math.cos(data.theta) - 1;
    },
    
    // scales the quotients to the units movespeed
    scaleQuotientsByMoveSpeed: function (data, unitArray) {
        data.xQuotient *= getUnitMoveSpeed(unitArray);
        data.zQuotient *= getUnitMoveSpeed(unitArray);
    },
    
    scaleQuotientsByScalar: function (data, scalar) {
        data.xQuotient *= scalar;
        data.zQuotient *= scalar;
    },
    // updates halfway point projections
    updateHalfProjections: function (data) {
        data.projectedXHalf = data.x + data.xQuotient * 0.5;
        data.projectedZHalf = data.z + data.zQuotient * 0.5;
        
        data.projectedXHalfQuad = ~~(data.projectedXHalf * 0.0625);
        data.projectedZHalfQuad = ~~(data.projectedZHalf * 0.0625);
    },
    
    // updates end point projections
    updateProjections: function(data) {
        data.projectedX = (data.x + data.xQuotient);
        data.projectedZ = (data.z + data.zQuotient);
        
        data.projectedXQuad = ~~(data.projectedX * 0.0625);
        data.projectedZQuad = ~~(data.projectedZ * 0.0625);
    },
    
    // detects whether a half way collision has occurred and updates the projected positions
    detectHalfCollisions: function(peerID, data, unitArray) {
        for (var i = 0; i < mapArray[data.projectedXHalfQuad][data.projectedZHalfQuad][peerID].length; i++) {
            var unitIDFound = mapArray[data.projectedXHalfQuad][data.projectedZHalfQuad][peerID][i];
            var unitArrayFound = allUnits[unitIDFound];
            
            if (data.thetaAdjustment >= 6 || data.thetaAdjustment <= -6) { // if full circle of adjustments, unit is surrounded, dont move
                data.xQuotient = 0;
                data.zQuotient = 0;
                break;
            }
            
            if (unitIDFound === getUnitID(unitArray)) { // stop unit avoiding itself
                continue;
            } else if (radiiAreTouching(data.projectedXHalf, data.projectedZHalf, getUnitRadius(unitArray), getUnitX(unitArrayFound), getUnitZ(unitArrayFound), getUnitRadius(unitArrayFound))) { // is this unit inside a unit
                
                var gradient = ((data.projectedZHalf - data.z) / (data.projectedXHalf - data.x)); // get units movement gradient
                var zIntercept = data.z - gradient * data.x; // get the z intercept
                var projectionZIntercept = zIntercept + gradient * getUnitX(unitArrayFound); // get z at collision x
                
                // determines if the unit should move clockwise or not around another unit
                if (projectionZIntercept >= getUnitZ(unitArrayFound)) { //clockwise
                    
                    if (data.thetaAdjustment === 0) {
                        data.theta += 0.5 * ++data.thetaAdjustment; // begin clockwise adjustment
                    } else if (data.thetaAdjustment < 0) {
                        data.theta -= 0.5 * --data.thetaAdjustment; // continue anticlockwise
                    } else {
                        data.theta += 0.5 * ++data.thetaAdjustment; // continue clockwise
                    }
                    
                } else { // anticlockwise
                    
                    if (data.thetaAdjustment === 0) {
                        data.theta -= 0.5 * --data.thetaAdjustment; // begin anticlockwise adjustment
                    } else if (thetaAdjustment > 0) {
                        data.theta += 0.5 * ++data.thetaAdjustment; // continue clockwise
                    } else {
                        data.theta -= 0.5 * --data.thetaAdjustment; // continue anticlockwise
                    }
                    
                }
                
                moveUnit.updateQuotients(data); // update all projects and estimates
                moveUnit.scaleQuotientsByMoveSpeed(data, unitArray);
                moveUnit.updateHalfProjections(data);
                i = 0; // reset the loop in case of new collisions from adjustments
                
            }
        }
    },
    
    //detects whether a full distance collision has occurred and updates the projected positions
    detectCollisions: function(peerID, data, unitArray) {
        for (var i = 0; i < mapArray[data.projectedXQuad][data.projectedZQuad][peerID].length; i++) {
            var unitIDFound = mapArray[data.projectedXQuad][data.projectedZQuad][peerID][i];
            var unitArrayFound = allUnits[unitIDFound];
            
            if (data.thetaAdjustment >= 12 || data.thetaAdjustment <= -12) {
                data.xQuotient = 0;
                data.zQuotient = 0;
                break;
            }
            
            if (unitIDFound === getUnitID(unitArray)) { // ignore this unit
                continue;
            } else if (radiiAreTouching(data.projectedX, data.projectedZ, getUnitRadius(unitArray), getUnitX(unitArrayFound), getUnitZ(unitArrayFound), getUnitRadius(unitArrayFound))) {
                
                var gradient = ((data.projectedZ - data.z) / (data.projectedX - data.x));
                var zIntercept = data.z - gradient * data.x;
                var projectionIntercept = zIntercept + gradient * getUnitX(unitArrayFound);
                
                if (projectionIntercept >= getUnitZ(unitArrayFound)) {
                    
                    if (data.thetaAdjustment === 0) {
                        data.theta += 0.5;
                        ++data.thetaAdjustment;
                    } else if (data.thetaAdjustment < 0) {
                        data.theta -= 0.5;
                        --data.thetaAdjustment;
                    } else {
                        data.theta += 0.5;
                        ++data.thetaAdjustment;
                    }
                    
                } else {
                    
                    // if (data.thetaAdjustment <= 0) {
                    //     data.theta -= 0.5 * --data.thetaAdjustment;
                    // } else if (data.thetaAdjustment > 0) {
                    //     data.theta += 0.5 * ++data.thetaAdjustment;
                    // }
                    
                    if (data.thetaAdjustment <= 0) {
                        data.theta -= 0.5;
                        --data.thetaAdjustment;
                    } else if (data.thetaAdjustment > 0) {
                        data.theta += 0.5;
                        ++data.thetaAdjustment;
                    }
                    
                }
                
                moveUnit.updateQuotients(data);
                moveUnit.scaleQuotientsByMoveSpeed(data, unitArray);
                moveUnit.updateProjections(data);
                i = 0;
                
            }
        }
    }
    
};

/*
 Error responses
 */

var respondToClient = {
    
    sendSpawnTimer: function(spawnRate){
        RTSession.newPacket()
            .setOpCode(91)
            .setData(RTSession.newData()
                .setNumber(1, spawnRate))
            .setReliable(true)
            .send();
    },
    sendUpdatedPeerResourcesToPeer: function (peerID) {
        RTSession.newPacket()
            .setOpCode(210)
            .setData(RTSession.newData()
                .setNumber(1, playerResources[peerID].GOLD)
                .setNumber(2, playerResources[peerID].LUMBER))
            .setReliable(true)
            .setTargetPeers(peerID)
            .send(); // send a packet to sender confirming unit addition and new resource count
    },
    sendNoUnitsToRemoveToPeer: function (peerID) {
        RTSession.newPacket()
            .setOpCode(403)
            .setTargetPeers(peerID)
            .send(); // send a packet to peers indicating new unlocked unit count
    },
    sendInsufficientResourcesToPeer: function (peerID){
        RTSession.newPacket()
            .setOpCode(410)
            .setTargetPeers(peerID)
            .send(); // send a packet to peers indicating new unlocked unit count
    },
    sendEquipmentChangeDeniedToPeer: function (peerID, unitCode, weaponCode, armourCode, requestID){
        RTSession.newPacket()
            .setOpCode(408)
            .setData(RTSession.newData()
                .setString(1, unitCode)
                .setString(2, weaponCode)
                .setString(3, armourCode)
                .setNumber(4, requestID))
            .setTargetPeers(peerID)
            .send();
    }
    
};

var gameover = {
    
    calculateEarnings: function(playerOneID, playerTwoID, playerOneDidWin){
        
        var playerTwoElo;
        var playerOneElo;
        
        RTSession.newRequest().createLogEventRequest().setEventKey("getTrophy").setPlayerId(playerTwoID).send(function(response){
            
            playerTwoElo = response.scriptData.trophy;
            _calculateEarnings();
            
        });
        
        RTSession.newRequest().createLogEventRequest().setEventKey("getTrophy").setPlayerId(playerOneID).send(function(response){
            
            playerOneElo = response.scriptData.trophy;
            _calculateEarnings();
            
        });
        
        function _calculateEarnings(){
            
            if (!(playerTwoElo || playerOneElo)){
                return;
            }
            
            var playerOneR = Math.pow(10, ~~(playerOneElo / 500));
            var playerTwoR = Math.pow(10, ~~(playerTwoElo / 500));
            
            var playerOneExpectedOutcome = playerOneR / (playerOneR + playerTwoR);
            var playerTwoExpectedOutcome = playerTwoR / (playerTwoR + playerOneR);
            
            var EloSensitivity = 32;
            
            var playerOneActualOutcome = playerOneDidWin ? (1 - playerOneExpectedOutcome) : (0 - playerOneExpectedOutcome);
            var playerTwoActualOutcome = !playerOneDidWin ? (1 - playerTwoExpectedOutcome) : (0 - playerTwoExpectedOutcome);
            
            playerOneElo = playerOneElo + ~~(EloSensitivity * (playerOneActualOutcome));
            playerTwoElo = playerTwoElo + ~~(EloSensitivity * (playerTwoActualOutcome));
            
            setPlayerEarnings(playerOneID, playerOneDidWin ? earnings.winner_gold : earnings.loser_gold, playerOneDidWin ? earnings.winner_lumber : earnings.loser_lumber, playerOneElo);
            setPlayerEarnings(playerTwoID, !playerOneDidWin ? earnings.winner_gold : earnings.loser_gold, !playerOneDidWin ? earnings.winner_lumber : earnings.loser_lumber, playerTwoElo);
            
        }
        
        function setPlayerEarnings(ID, gold, lumber, trophy){
            RTSession.newRequest().createLogEventRequest().setEventKey("setEarnings").setPlayerId(ID).setScriptData({
                "gold": gold,
                "lumber": lumber,
                "trophy": trophy,
                "api_key": internalAPIKey
            }).send(function(response){
                
                if (response.error){
                    
                    RTSession.getLogger().debug("SET PLAYER EARNINGS BUG: ID: " + ID + " GOLD: " + gold + " LUMBER: " + lumber + " TROPHY: " + trophy);
                    
                }
                
            });
        }
        
    }
    
};


/*
 ------------------------
 TESTING
 ------------------------
 */

function testSetup(){
    
    playerIsReady = [1, 2];
    playerResources[1] = {
        "GOLD": 1000,
        "LUMBER": 1000
    };
    playerResources[2] = {
        "GOLD": 1000,
        "LUMBER": 1000
    };
    playerInventory[1] = {
        "weapons": {
            "sword": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "axe": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "spear": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "short_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "long_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "cross_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fire_bolt": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "ice_shard": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "chain_lightning": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "scimitar": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "lance": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "mace": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "regular_shot": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "multi_shot": {
                "level": 1,"max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "flame_shot": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "large_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "chain_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "burst_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "life_steal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "convert": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "health_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "speed_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "damage_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "thorns_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fort_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "tower_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "trap_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            }
        }, "armour": {
            "light": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "medium": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "heavy": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fortified": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            }
        }, "units": {
            "zombie": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "convert",
                "armour": "light",
                "available_armour": ["light", "medium", "heavy"],
                "available_weapons": ["life_steal", "convert"]
            },
            "healer": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "heal_large",
                "armour": "light",
                "available_weapons": ["large_heal", "chain_heal", "burst_heal"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "catapult": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "regular_shot",
                "armour": "light",
                "available_weapons": ["regular_shot", "multi_shot", "flame_shot"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "monk": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "health_aura",
                "armour": "light",
                "available_weapons": ["health_aura", "speed_aura", "damage_aura", "thorns_aura"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "cavalry": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "scimitar",
                "armour": "light",
                "available_weapons": ["lance", "mace", "scimitar"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "wizard": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "fire_bolt",
                "armour": "light",
                "available_weapons": ["fire_bolt", "ice_shard", "chain_lightning"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "archer": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "short_bow",
                "armour": "light",
                "available_weapons": ["short_bow", "long_bow", "cross_bow"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "warrior": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "sword",
                "armour": "light",
                "available_weapons": ["sword", "axe", "spear"],
                "available_armour": ["light", "medium", "heavy"]
            }
        }, "defenses": {
            "command_centre": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "tower": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "tower_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["tower_basic_attack"],
                "available_armour": ["fortified"]
            },
            "fort": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "fort_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["fort_basic_attack"],
                "available_armour": ["fortified"]
            },
            "explosive_trap": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "trap_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["trap_basic_attack"],
                "available_armour": ["fortified"]
            },
            "gold_mine": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "lumber_mill": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "road": {
                "level": 1,
                "max_level": 1,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            }
        }};
    playerInventory[2] = {
        "weapons": {
            "sword": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "axe": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "spear": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "short_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "long_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "cross_bow": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fire_bolt": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "ice_shard": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "chain_lightning": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "scimitar": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "lance": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "mace": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "regular_shot": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "multi_shot": {
                "level": 1,"max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "flame_shot": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "large_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "chain_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "burst_heal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "life_steal": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "convert": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "health_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "speed_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "damage_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "thorns_aura": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fort_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "tower_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "trap_basic_attack": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            }
        }, "armour": {
            "light": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "medium": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "heavy": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            },
            "fortified": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0
            }
        }, "units": {
            "zombie": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "convert",
                "armour": "light",
                "available_armour": ["light", "medium", "heavy"],
                "available_weapons": ["life_steal", "convert"]
            },
            "healer": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "heal_large",
                "armour": "light",
                "available_weapons": ["large_heal", "chain_heal", "burst_heal"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "catapult": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "regular_shot",
                "armour": "light",
                "available_weapons": ["regular_shot", "multi_shot", "flame_shot"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "monk": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "health_aura",
                "armour": "light",
                "available_weapons": ["health_aura", "speed_aura", "damage_aura", "thorns_aura"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "cavalry": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "scimitar",
                "armour": "light",
                "available_weapons": ["lance", "mace", "scimitar"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "wizard": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "fire_bolt",
                "armour": "light",
                "available_weapons": ["fire_bolt", "ice_shard", "chain_lightning"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "archer": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "short_bow",
                "armour": "light",
                "available_weapons": ["short_bow", "long_bow", "cross_bow"],
                "available_armour": ["light", "medium", "heavy"]
            },
            "warrior": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "sword",
                "armour": "light",
                "available_weapons": ["sword", "axe", "spear"],
                "available_armour": ["light", "medium", "heavy"]
            }
        }, "defenses": {
            "command_centre": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "tower": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "tower_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["tower_basic_attack"],
                "available_armour": ["fortified"]
            },
            "fort": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "fort_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["fort_basic_attack"],
                "available_armour": ["fortified"]
            },
            "explosive_trap": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "weapon": "trap_basic_attack",
                "armour": "fortified_armour",
                "available_weapons": ["trap_basic_attack"],
                "available_armour": ["fortified"]
            },
            "gold_mine": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "lumber_mill": {
                "level": 1,
                "max_level": 3,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            },
            "road": {
                "level": 1,
                "max_level": 1,
                "is_upgrading": false,
                "upgradeComplete": 0,
                "upgradeBegin": 0,
                "armour": "fortified_armour",
                "available_armour": ["fortified"]
            }
        }};
    playerDraftsLocked[1] = {
        "total": 0,
        "isLocked": false
    };
    playerDraftsLocked[2] = {
        "total": 0,
        "isLocked": false
    };
    
    playerDrafts = {};
    playerDrafts[1] = {};
    playerDrafts[2] = {};
    
    playerDecks[waveVersion] = {};
    playerDecks[waveVersion][1] = {};
    playerDecks[waveVersion][2] = {};
    spawnRateInverse = 1 / 20000;
    nextSpawnPeriod = Date.now();
    
    allUnits = {};
    
    initialisePlayerDeck(1);
    initialisePlayerDeck(2);
    constructMapNodes();
    constructMapArray();
    constructBuildNodes();
    setupCommandCentres();
}

function draftUnitSim(id, unitType){
    
    var peerID = id;
    var unitName = "warrior";
    
    if (unitType){
        unitName = unitType;
    }
    
    if (!playerDraftsLocked[peerID].isLocked &&
        playerDraftsLocked[peerID].total < 64){ // check for unit being locked
        
        if (Object.keys(playerDecks[waveVersion][peerID]).indexOf(unitName) !== -1){ // check player is allowed to draft unit
            var currentGold = playerResources[peerID].GOLD;
            var currentLumber = playerResources[peerID].LUMBER;
            
            var unitGoldCost = playerDecks[waveVersion][peerID][unitName].GOLD;
            var unitLumberCost = playerDecks[waveVersion][peerID][unitName].LUMBER;
            
            if (currentGold >= unitGoldCost && currentLumber >= unitLumberCost){
                
                playerResources[peerID].GOLD -= unitGoldCost;
                playerResources[peerID].LUMBER -= unitLumberCost;
                
                if (!playerDrafts[peerID][unitName]){
                    playerDrafts[peerID][unitName] = 0;
                }
                
                playerDrafts[peerID][unitName]++;
                playerDraftsLocked[peerID].total++;
                
                var sendTo = playerIsReady.slice(0);
                sendTo.splice(sendTo.indexOf(peerID), 1);
                
                
            }
            
        }
    }
    
}

function buildStructureSim(peerID, structureCode, siteNumber) {
    
    var buildSite = buildSites[siteNumber];
    
    // console.log("here");
    
    if (playerDecks[waveVersion][peerID][structureCode] &&
        buildSite &&
        buildSite.owned_by === peerID &&
        buildSite.structure === "none"){
        
        // console.log("here");
        
        if (playerResources[peerID].GOLD >= playerDecks[waveVersion][peerID][structureCode].GOLD &&
            playerResources[peerID].LUMBER >= playerDecks[waveVersion][peerID][structureCode].LUMBER){
            
            buildSite.structure = structureCode;
            buildSite.completionDate = playerDecks[waveVersion][peerID][structureCode].CONSTRUCTION_TIME + Date.now();
            buildSite.beginDate = Date.now();
            
            // console.log("here");
            // console.log(unitIDCounter);
            
            allUnits[unitIDCounter] = [peerID,
                unitIDCounter,
                0,
                buildSite.siteReference,
                0,
                buildSite.x,
                buildSite.y,
                buildSite.z,
                structureCode,
                playerDecks[waveVersion][peerID][structureCode].HP * (1 / playerDecks[waveVersion][peerID][structureCode].CONSTRUCTION_TIME),
                0,
                0,
                waveVersion,
                [],
                [],
                structureRadius,
                buildStructure];
            
            playerResources[peerID].GOLD -= playerDecks[waveVersion][peerID][structureCode].GOLD;
            playerResources[peerID].LUMBER -= playerDecks[waveVersion][peerID][structureCode].LUMBER;
            
            buildSite.unitID = unitIDCounter++;
            
            // console.log("xQuad: " + buildSite.xQuad + " zQuad: " + buildSite.zQuad);
            mapArray[buildSite.xQuad][buildSite.zQuad][peerID].push(buildSite.unitID);
            
            
        }
        
    }
    
}

function updateGameStateSim(){
    
    
    // var unitData = RTSession.newData();
    deadUnits = [];
    var buffer = 0;
    
    var unitIDS = Object.keys(allUnits);
    
    for(var i = 0; i < unitIDS.length; i++){ //update all units & structures
        
        if (buffer == 10){
            // RTSession.newPacket().setOpCode(201).setData(unitData).send();
            buffer = 0;
            // unitData = RTSession.newData();
        }
        
        var unit = allUnits[unitIDS[i]]; // get the data array for that unit
        
        allUnits[unitIDS[i]] = unit[16](unit); // run the referenced function to update the unit and store the update
        
        //  allUnits[unitIDS[i]][16](allUnits[unitIDS[i]]);
        
        // unitData.setString(++buffer,  allUnits[unitIDS[i]].slice(0, 14).join(":")); // add the updated unit data into the packet buffer
        
    }
    
    // console.log(allUnits);
    
    // update build sites
    buildSites.forEach(function(site){
        
        if (site.structure === "none"){
            
            var peer1IsNearby = scanForTarget(1, site.xQuad, site.zQuad);
            var peer2IsNearby = scanForTarget(2, site.xQuad, site.zQuad);
            
            if (peer1IsNearby !== peer2IsNearby){
                site.owned_by = peer1IsNearby ? 1 : 2;
                // RTSession.newPacket()
                //     .setOpCode(209)
                //     .setData(RTSession.newData()
                //         .setNumber(1, site.owned_by)
                //         .setNumber(2, site.siteReference))
                //     .setReliable(true)
                //     .send();
            }
            
        }
        
    });
    
    updateUnitManifest();
    
    if (deadUnits.length != 0){
        // RTSession.newPacket()
        //     .setOpCode(207)
        //     .setData(RTSession.newData().setFloatArray(1, deadUnits))
        //     .setReliable(true)
        //     .send();
    }
    
    playerIsReady.forEach(function (peerID) {
        // respondToClient.sendUpdatedPeerResourcesToPeer(peerID);
    });
    
    var peer1 = allUnits[commandCentres[0].unitID];
    var peer2 = allUnits[commandCentres[1].unitID];
    
    if (!peer1 && !peer2){
        
        var drawGold = 250;
        var drawLumber = 250;
        
        console.log("\n\nGAME OVER!\n\n");
        console.log("DRAW!");
        
        // RTSession.newPacket()
        //     .setOpCode(11)
        //     .setData(RTSession.newData()
        //         .setNumber(1, drawGold)
        //         .setNumber(2, drawLumber))
        //     .setReliable(true)
        //     .send();
        
    } else if (!peer1 || !peer2){
        
        var winner = !peer1 ? 2 : 1;
        var loser = !peer1 ? 1 : 2;
        
        var winnerGold = 500;
        var winnerLumber = 500;
        
        var loserGold = 150;
        var loserLumber = 150;
        
        console.log("\n\nGAME OVER!\n\n");
        console.log("winner: " + winner);
        
        
    }
    
}

function spawnUnitsSim(){
    
    nextSpawnPeriod += spawnRate;
    
    playerIsReady.forEach(function(peerID){
        
        var x = peerID == 1 ? 405 : 127.5;
        var y = 25;
        var z = peerID == 1 ? 65 : 426;
        var direction = peerID == 1 ? -1 : 1;
        var firstNode = peerID == 1 ? 5 : 1;
        var opposingPlayerID = peerID == 1 ? 2 : 1;
        var countRef = 1;
        
        var zRow = 0;
        
        var units = Object.keys(playerDrafts[peerID]);
        
        if (units.length > 0){
            
            units.forEach(function(unitCode){
                
                var attackFunction = getAttackFunction(peerID, unitCode);
                var targetFunction = getTargetFunction(unitCode);
                var updateFunction = getUpdateFunction(unitCode);
                var targetCheckerFunction = getTargetCheckerFunction(unitCode);
                
                for (var i = 0; i < playerDrafts[peerID][unitCode]; i++){
                    
                    allUnits[unitIDCounter] = [peerID,                              //0
                        (unitIDCounter++),                                          //1
                        direction,                                                  //2
                        firstNode,                                                  //3
                        0,                                                          //4
                        x,                                                          //5
                        y,                                                          //6
                        z,                                                          //7
                        unitCode,                                                   //8
                        playerDecks[waveVersion][peerID][unitCode].HP,              //9
                        0,                                                          //10
                        0,                                                          //11
                        waveVersion,                                                //12
                        [],                                                         //13
                        [],
                        creepRadius,
                        updateFunction,                                             //15
                        targetFunction,                                             //16
                        attackFunction,                                             //17
                        targetCheckerFunction,                                      //18
                        playerDecks[waveVersion][peerID][unitCode].RANGE,           //19
                        playerDecks[waveVersion][peerID][unitCode].MOVEMENT_SPEED,  //20
                        playerDecks[waveVersion][peerID][unitCode].ATTACK_SPEED,    //21
                        playerDecks[waveVersion][peerID][unitCode].DAMAGE,          //22
                        playerDecks[waveVersion][peerID][unitCode].AOE_RADIUS,      //23
                        playerDecks[waveVersion][peerID][unitCode].HIT_LIMIT];      //24
                    
                    // spawns units in a 8 x n grid
                    if (zRow == spawnGridLength){
                        zRow = 0;
                        z += 2;
                        x -= (spawnGridLength * 2);
                    } else {
                        x += 2;
                        zRow++;
                    }
                    
                }
                
            });
        }
        
        // reset
        playerDrafts[peerID] = {};
        playerDraftsLocked[peerID] = {
            "total": 0,
            "isLocked": false
        };
        
    });
    
}

function testBasicCombat(){
    unitIDCounter = 1;
    testSetup();
    draftUnitSim(1);
    draftUnitSim(2);
    spawnUnitsSim();
}

function testCreepAttackCommandCentre() {
    unitIDCounter = 1;
    testSetup();
    draftUnitSim(1);
    spawnUnitsSim();
}

function initHealerTest_singleTargetLargeHeal() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    draftUnitSim(1, "healer");
    draftUnitSim(1, "warrior");
    spawnUnitsSim();
    allUnits[2][9] = 20;
}

function initHealerTest_chainHeal() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    playerInventory[1].units.healer.weapon = "chain_heal";
    draftUnitSim(1, "healer");
    draftUnitSim(1, "warrior");
    draftUnitSim(1, "warrior");
    draftUnitSim(1, "warrior");
    draftUnitSim(1, "warrior");
    draftUnitSim(1, "warrior");
    spawnUnitsSim();
    allUnits[2][9] = 20; // remove warrior HP
    allUnits[3][9] = 20;
    allUnits[4][9] = 20;
    allUnits[5][9] = 20;
    allUnits[1][24] = 20; // increase healer AOE radius
    allUnits[1][21] = 20; // increase healer move speed
    
}

function initHealerTest_burstHeal() {
    
}

function testCollisionAvoidance() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    buildSites[3].owned_by = 2;
    buildStructureSim(2, "lumber_mill", 3);
    allUnits[1][9] = 99;
    updateGameStateSim();
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    spawnUnitsSim();
    allUnits[1][9] = 10000;
    // successful test should have all units attack the structure on different x : z coords.
}

function testTowerAttack() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    buildSites[3].owned_by = 2;
    buildStructureSim(2, "tower", 3);
    allUnits[1][9] = 99;
    updateGameStateSim();
    draftUnitSim(1);
    spawnUnitsSim();
}

function testTrapAttack() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    buildSites[3].owned_by = 2;
    buildStructureSim(2, "explosive_trap", 3);
    allUnits[1][9] = 99;
    updateGameStateSim();
    allUnits[1][9] = 1000;
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    //successful test should have majority of units injured
    spawnUnitsSim();
}

function testFortAttack() {
    unitIDCounter = 1;
    testSetup();
    allUnits = {};
    buildSites[3].owned_by = 2;
    buildStructureSim(2, "fort", 3);
    allUnits[1][9] = 99;
    updateGameStateSim();
    allUnits[1][9] = 1000;
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    draftUnitSim(1);
    spawnUnitsSim();
}

function calculateUnitDistances() {
    
    var errors = 0;
    
    for(var i = 2; i < 8; i++){
        
        var x = getUnitX(allUnits[i]);
        var z = getUnitZ(allUnits[i]);
        
        var averageDistance = 0;
        
        for(var j = 2; j < 8; j++){
            
            if (i !== j){
                
                var testX = getUnitX(allUnits[j]);
                var testZ = getUnitZ(allUnits[j]);
                
                var length = Math.sqrt(((testX - x) * (testX - x)) + ((testZ - z) * (testZ - z)));
                
                if (length < creepRadius){
                    errors++;
                }
                
                averageDistance += length;
                
            }
            
        }
        
        console.log("average distance for unit " + getUnitID(allUnits[i]) + ": " + averageDistance / 7);
        
    }
    
    console.log("boundary errors: " + errors);
    
}

function testFloatingPointPrecision(){
    
    allUnits = {};
    testSetup();
    
    for(var i = 0; i < 4; i++) {
        draftUnitSim(1, "archer");
        draftUnitSim(2, "archer");
    }
    
    for(var i = 0; i < 6; i++) {
        draftUnitSim(1);
    }
    
    for(var i = 0; i < 3; i++) {
        draftUnitSim(2);
    }
    
    spawnUnitsSim();
    
}